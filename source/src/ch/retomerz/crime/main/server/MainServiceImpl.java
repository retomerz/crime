package ch.retomerz.crime.main.server;

import ch.retomerz.crime.main.client.util.Base64Encoder;
import ch.retomerz.crime.main.client.zlib.com.jcraft.jzlib.DeflaterOutputStream;
import ch.retomerz.crime.main.server.util.DisposeUtil;
import ch.retomerz.crime.main.server.util.StreamUtil;
import ch.retomerz.crime.main.server.util.ThrowableUtil;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import ch.retomerz.crime.main.client.MainService;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public final class MainServiceImpl extends RemoteServiceServlet implements MainService {

    @Override
    public String getResourceUrl(String _Name) {
        if ("testimg".equals(_Name)) {
            //return getServletContext().getContextPath() + "/crime/model/test/palm.png";
            //return getServletContext().getContextPath() + "/crime/model/test/blocks11b.png";
            return getServletContext().getContextPath() + "/crime/textures/webtreats_stone_4_512.png";
        } else {
          return getServletContext().getContextPath() + "/crime/" + _Name + ".png";
        }
    }

    @Override
    public byte[] getByteData(String path) {
        return read("stam.obj");
    }

    @Override
    public String getBSP(String name) {
        final File file = new File("C:\\git\\crime\\resource\\crime_test1.bsp");
        //final File file = new File("C:\\git\\crime\\resource\\q3tourney2.bsp");
        //final File file = new File("C:\\git\\crime\\resource\\m3f.bsp");
        try {
            final FileInputStream in = new FileInputStream(file);
            try {
                final byte[] map = read(in);
                //final ch.retomerz.crime.main.client.jre.java.io.ByteArrayOutputStream out = new ch.retomerz.crime.main.client.jre.java.io.ByteArrayOutputStream();
                //DeflaterOutputStream def = new DeflaterOutputStream(out);
                //def.write(map);
                final String retOld = Base64Encoder.encodeToString(map);
                //final String ret = Base64Encoder.encodeToString(out.toByteArray());
                return retOld;
            } finally {
                DisposeUtil.unsafeClose(in);
            }
        } catch (IOException e) {
            throw ThrowableUtil.toUnchecked(e);
        }
    }

    private static byte[] read(String _Name) {
        final InputStream in = MainServiceImpl.class.getResourceAsStream(_Name);
        try {
            return read(in);
        } finally {
            DisposeUtil.unsafeClose(in);
        }
    }
    private static byte[] read(InputStream _Input) {
        try {
            final ByteArrayOutputStream out = new ByteArrayOutputStream();
            StreamUtil.copy(_Input, out);
            return out.toByteArray();
        } catch (IOException e) {
            throw ThrowableUtil.toUnchecked(e);
        }
    }
}