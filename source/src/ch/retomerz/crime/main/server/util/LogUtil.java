/**
 * Created by merz on 27.01.14.
 */
package ch.retomerz.crime.main.server.util;

import java.util.logging.Logger;

public final class LogUtil {

    private LogUtil() {
    }

    public static Logger getLogger(Class clazz) {
        return Logger.getLogger(clazz.getName());
    }
}
