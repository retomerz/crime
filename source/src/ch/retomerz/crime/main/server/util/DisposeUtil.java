/**
 * Created by merz on 27.01.14.
 */
package ch.retomerz.crime.main.server.util;

import java.io.Closeable;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class DisposeUtil {

    private static final Logger LOG = LogUtil.getLogger(DisposeUtil.class);

    private DisposeUtil() {
    }

    public static void unsafeClose(Closeable closeable) {
        try {
            closeable.close();
        } catch (IOException e) {
            LOG.log(Level.INFO, null, e);
        }
    }
}
