/**
 * Created by merz on 21.04.14.
 */
package ch.retomerz.crime.main.server.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public final class StreamUtil {

    private StreamUtil() {
    }

    public static void copy(InputStream in, OutputStream out) throws IOException {
        final byte[] buf = new byte[8*1024];
        int read;
        while (-1 != (read = in.read(buf))) {
            if (read > 0) {
                out.write(buf, 0, read);
            } else {
                Thread.yield();
            }
        }
    }

}