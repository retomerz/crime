/**
 * Created by merz on 27.01.14.
 */
package ch.retomerz.crime.main.server.util;

public final class ThrowableUtil {

    private ThrowableUtil() {
    }

    public static RuntimeException toUnchecked(Throwable e) {
        if (e instanceof RuntimeException) {
            return (RuntimeException)e;
        } else {
            return new RuntimeException(e);
        }
    }
}
