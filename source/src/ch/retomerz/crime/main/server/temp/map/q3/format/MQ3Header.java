/**
 * Created by merz on 01.05.14.
 */
package ch.retomerz.crime.main.server.temp.map.q3.format;

public final class MQ3Header {
    public char[/* 4 */] m_MagicNumber;     // Magic number. Always "IBSP".
    public int m_Version;                   // Version number 0x2e for the BSP files distributed with Quake 3.
    public MQ3Lump[/* 17 */] m_Lumpes;      // Lump directory, seventeen entries.
}