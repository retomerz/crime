/**
 * Created by merz on 01.05.14.
 */
package ch.retomerz.crime.main.server.temp.map.q3.format;

public final class MQ3Lump {
    public int m_Offset; // Offset to start of lump, relative to beginning of file.
    public int m_Length; // Length of lump. Always a multiple of 4.
}