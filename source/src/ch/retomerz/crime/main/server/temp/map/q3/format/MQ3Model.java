/**
 * Created by merz on 01.05.14.
 */
package ch.retomerz.crime.main.server.temp.map.q3.format;

import ch.retomerz.crime.main.server.temp.map.q3.LEDByteAccess;

import java.util.Arrays;

public final class MQ3Model {
    public float[/* 3 */] m_Mins;    // Bounding box min coord.
    public float[/* 3 */] m_Maxs;    // Bounding box max coord.
    public int m_Face;               // First face for model.
    public int m_NbFaces;            // Number of faces for model.
    public int m_Brush;              // First brush for model.
    public int m_NBrushes;           // Number of brushes for model.

    @Override
    public String toString() {
        return "MQ3Model{" +
                "mins=" + Arrays.toString(m_Mins) +
                ", maxs=" + Arrays.toString(m_Maxs) +
                ", face=" + m_Face +
                ", nbFaces=" + m_NbFaces +
                ", brush=" + m_Brush +
                ", nBrushes=" + m_NBrushes +
                '}';
    }

    public static final int SIZE_OF =
            (3*LEDByteAccess.SIZE_OF_FLOAT) +
            (3*LEDByteAccess.SIZE_OF_FLOAT) +
            LEDByteAccess.SIZE_OF_INT +
            LEDByteAccess.SIZE_OF_INT +
            LEDByteAccess.SIZE_OF_INT +
            LEDByteAccess.SIZE_OF_INT;

}