/**
 * Created by merz on 01.05.14.
 */
package ch.retomerz.crime.main.server.temp.map.q3.format;

import ch.retomerz.crime.main.server.temp.map.q3.LEDByteAccess;

import java.util.Arrays;

public final class MQ3Node {
    public int m_Plane; // Plane index.
    public int[/* 2 */] m_Children; // Children indices. Negative numbers are leaf indices: -(leaf+1).
    public int[/* 3 */] m_Mins; // Integer bounding box min coord.
    public int[/* 3 */] m_Maxs; // Integer bounding box max coord.

    @Override
    public String toString() {
        return "MQ3Node{" +
                "plane=" + m_Plane +
                ", children=" + Arrays.toString(m_Children) +
                ", mins=" + Arrays.toString(m_Mins) +
                ", maxs=" + Arrays.toString(m_Maxs) +
                '}';
    }

    public static final int SIZE_OF =
            LEDByteAccess.SIZE_OF_INT +
            (2*LEDByteAccess.SIZE_OF_INT) +
            (3*LEDByteAccess.SIZE_OF_INT) +
            (3*LEDByteAccess.SIZE_OF_INT);

}