/**
 * Created by merz on 04.05.14.
 */
package ch.retomerz.crime.main.server.temp.map.q3;

import java.io.IOException;

public final class LEDByteAccess {

    public static final int SIZE_OF_CHAR = 1;
    public static final int SIZE_OF_INT = 4;
    public static final int SIZE_OF_FLOAT = 4;

    private final byte[] m_Data;
    private final byte[] m_Buffer = new byte[8];
    private int m_Pos;

    public LEDByteAccess(byte [] _Data) {
        m_Data = _Data;
        m_Pos = 0;
    }

    public void seek(int _Pos) throws IOException {
        m_Pos = _Pos;
    }

    private byte readByte() throws IOException {
        return m_Data[m_Pos++];
    }

    public char[] readChar(int _Count) throws IOException {
        final char[] ret = new char[_Count];
        for (int i = 0; i < _Count; i++) {
            ret[i] = (char)readByte();
        }
        return ret;
    }

    // TODO unsigned ?
    public char[] readCharUnsigned(int _Count) throws IOException {
        final char[] ret = new char[_Count];
        for (int i = 0; i < _Count; i++) {
            ret[i] = (char)readByte();
        }
        return ret;
    }

    // TODO unsigned ?
    // TODO a b c order correct ?
    public char[][][] readChar3Unsigned(int _CountA, int _CountB, int _CountC) throws IOException {
        final char[][][] ret = new char[_CountA][_CountB][_CountC];
        for (int a = 0; a < _CountA; a++) {
            for (int b = 0; b < _CountB; b++) {
                for (int c = 0; c < _CountC; c++) {
                    ret[a][b][c] = (char)readByte();
                }
            }
        }
        return ret;
    }

    public float readFloat() throws IOException {
        return Float.intBitsToFloat(readInt());
    }

    public float[] readFloat(int _Count) throws IOException {
        final float[] ret = new float[_Count];
        for (int i = 0; i < _Count; i++) {
            ret[i] = readFloat();
        }
        return ret;
    }

    // TODO
    public float[][] readFloat2(int _CountA, int _CountB) throws IOException {
        final float[][] ret = new float[_CountA][_CountB];
        for (int a = 0; a < _CountA; a++) {
            for (int b = 0; b < _CountB; b++) {
                ret[a][b] = readFloat();
            }
        }
        return ret;
    }

    public int readInt() throws IOException {
        m_Buffer[0] = readByte();
        m_Buffer[1] = readByte();
        m_Buffer[2] = readByte();
        m_Buffer[3] = readByte();
        return (m_Buffer[3]) << 24 | (m_Buffer[2] & 0xff) << 16 | (m_Buffer[1] & 0xff) << 8 | (m_Buffer[0] & 0xff);
    }

    public int[] readInt(int _Count) throws IOException {
        final int[] ret = new int[_Count];
        for (int i = 0; i < _Count; i++) {
            ret[i] = readInt();
        }
        return ret;
    }

}