/**
 * Created by merz on 01.05.14.
 */
package ch.retomerz.crime.main.server.temp.map.q3.format;

import ch.retomerz.crime.main.server.temp.map.q3.LEDByteAccess;

import java.util.Arrays;

public final class MQ3Effect {
    public char[/* 64 */] m_Name; // Effect shader.
    public int m_Brush; // Brush that generated this effect.
    public int m_Unknown; // Always 5, except in q3dm8, which has one effect with -1.

    @Override
    public String toString() {
        return "MQ3Effect{" +
                "name=" + Arrays.toString(m_Name) +
                ", brush=" + m_Brush +
                ", unknown=" + m_Unknown +
                '}';
    }

    public static final int SIZE_OF =
            (64* LEDByteAccess.SIZE_OF_CHAR) +
            LEDByteAccess.SIZE_OF_INT +
            LEDByteAccess.SIZE_OF_INT;
}