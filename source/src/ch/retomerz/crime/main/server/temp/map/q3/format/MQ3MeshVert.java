/**
 * Created by merz on 01.05.14.
 */
package ch.retomerz.crime.main.server.temp.map.q3.format;

import ch.retomerz.crime.main.server.temp.map.q3.LEDByteAccess;

public final class MQ3MeshVert {
    public int m_MeshVert; // Vertex index offset, relative to first vertex of corresponding face.

    @Override
    public String toString() {
        return "MQ3MeshVert{" +
                "meshVert=" + m_MeshVert +
                '}';
    }

    public static final int SIZE_OF = LEDByteAccess.SIZE_OF_INT;
}