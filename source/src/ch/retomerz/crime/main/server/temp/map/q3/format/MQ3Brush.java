/**
 * Created by merz on 01.05.14.
 */
package ch.retomerz.crime.main.server.temp.map.q3.format;

import ch.retomerz.crime.main.server.temp.map.q3.LEDByteAccess;

public final class MQ3Brush {
    public int m_BrushSide; // First brushside for brush.
    public int m_NbBrushSides; // Number of brushsides for brush.
    public int m_TextureIndex; // Texture index.

    @Override
    public String toString() {
        return "MQ3Brush{" +
                "brushSide=" + m_BrushSide +
                ", nbBrushSides=" + m_NbBrushSides +
                ", textureIndex=" + m_TextureIndex +
                '}';
    }

    public static final int SIZE_OF =
            LEDByteAccess.SIZE_OF_INT +
            LEDByteAccess.SIZE_OF_INT +
            LEDByteAccess.SIZE_OF_INT;
}