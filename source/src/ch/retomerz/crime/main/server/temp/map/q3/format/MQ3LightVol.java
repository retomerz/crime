/**
 * Created by merz on 01.05.14.
 */
package ch.retomerz.crime.main.server.temp.map.q3.format;

import ch.retomerz.crime.main.server.temp.map.q3.LEDByteAccess;

import java.util.Arrays;

public final class MQ3LightVol {
    public /*unsigned*/ char[/* 3 */] m_Ambient; // Ambient color component. RGB
    public /*unsigned*/ char[/* 3 */] m_Directional; // Directional color component. RGB
    public /*unsigned*/ char[/* 2 */] m_Dir; // Direction to light. 0=phi, 1=theta

    @Override
    public String toString() {
        return "MQ3LightVol{" +
                "ambient=" + Arrays.toString(m_Ambient) +
                ", directional=" + Arrays.toString(m_Directional) +
                ", dir=" + Arrays.toString(m_Dir) +
                '}';
    }

    public static final int SIZE_OF =
            (3*LEDByteAccess.SIZE_OF_CHAR) +
            (3*LEDByteAccess.SIZE_OF_CHAR) +
            (2*LEDByteAccess.SIZE_OF_CHAR);

}