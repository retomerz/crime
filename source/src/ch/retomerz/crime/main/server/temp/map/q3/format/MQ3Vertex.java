/**
 * Created by merz on 01.05.14.
 */
package ch.retomerz.crime.main.server.temp.map.q3.format;

import ch.retomerz.crime.main.server.temp.map.q3.LEDByteAccess;

import java.util.Arrays;

public final class MQ3Vertex {
    public float[/* 3 */] m_Position;               // Vertex position.
    public float[/* 2 */][/* 2 */] m_TexCoord;      // Vertex texture coordinates. 0 = Surface, 1 = Lightmap.
    public float[/* 3 */] m_Normal;                 // Vertex normal.
    public /* unsigned */ char[/* 4 */] m_Color;    // Vertex color (RGBA).

    @Override
    public String toString() {
        return "MQ3Vertex{" +
                "position=" + Arrays.toString(m_Position) +
                ", texCoord=" + Arrays.toString(m_TexCoord) +
                ", normal=" + Arrays.toString(m_Normal) +
                ", color=" + Arrays.toString(m_Color) +
                '}';
    }

    public static final int SIZE_OF =
            (3*LEDByteAccess.SIZE_OF_FLOAT) +
            (2*2*LEDByteAccess.SIZE_OF_FLOAT) +
            (3*LEDByteAccess.SIZE_OF_FLOAT) +
            (4*LEDByteAccess.SIZE_OF_CHAR);
}