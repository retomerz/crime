/**
 * Created by merz on 01.05.14.
 */
package ch.retomerz.crime.main.server.temp.map.q3.format;

import java.util.ArrayList;
import java.util.List;

public class MQ3Map {

    public MQ3Header m_Header;
    public MQ3Entity m_Entity;
    public final List<MQ3Texture> m_Textures;
    public final List<MQ3Plane> m_Planes;
    public final List<MQ3Node> m_Nodes;
    public final List<MQ3Leaf> m_Leaves;
    public final List<MQ3LeafFace> m_LeafFaces;
    public final List<MQ3LeafBrush> m_LeafBrushes;
    public final List<MQ3Model> m_Models;
    public final List<MQ3Brush> m_Brushes;
    public final List<MQ3BrushSide> m_BrushSides;
    public final List<MQ3Vertex> m_Vertices;
    public final List<MQ3MeshVert> m_MeshVertices;
    public final List<MQ3Effect> m_Effects;
    public final List<MQ3Face> m_Faces;
    public final List<MQ3LightMap> m_LightMaps;
    public final List<MQ3LightVol> m_LightVols;
    public MQ3VisData m_VisData;

    public MQ3Map() {
        m_Textures = new ArrayList<>();
        m_Planes = new ArrayList<>();
        m_Nodes = new ArrayList<>();
        m_Leaves = new ArrayList<>();
        m_LeafFaces = new ArrayList<>();
        m_LeafBrushes = new ArrayList<>();
        m_Models = new ArrayList<>();
        m_Brushes = new ArrayList<>();
        m_BrushSides = new ArrayList<>();
        m_Vertices = new ArrayList<>();
        m_MeshVertices = new ArrayList<>();
        m_Effects = new ArrayList<>();
        m_Faces = new ArrayList<>();
        m_LightMaps = new ArrayList<>();
        m_LightVols = new ArrayList<>();
    }

}