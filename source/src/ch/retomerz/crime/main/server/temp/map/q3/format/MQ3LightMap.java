/**
 * Created by merz on 01.05.14.
 */
package ch.retomerz.crime.main.server.temp.map.q3.format;

import ch.retomerz.crime.main.server.temp.map.q3.LEDByteAccess;

import java.util.Arrays;

public final class MQ3LightMap {
    public /*unsigned*/ char[/* 128 */][/* 128 */][/* 3 */] m_MapData; // Lightmap color data. RGB

    @Override
    public String toString() {
        return "MQ3LightMap{" +
                "mapData=" + Arrays.toString(m_MapData) +
                '}';
    }

    public static final int SIZE_OF =
            (128*128*3* LEDByteAccess.SIZE_OF_CHAR);
}