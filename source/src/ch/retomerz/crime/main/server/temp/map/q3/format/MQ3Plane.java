/**
 * Created by merz on 01.05.14.
 */
package ch.retomerz.crime.main.server.temp.map.q3.format;

import ch.retomerz.crime.main.server.temp.map.q3.LEDByteAccess;

import java.util.Arrays;

public final class MQ3Plane {
    public float[/* 3 */] m_Normal;     // Plane normal.
    public float m_Distance;            // Distance from origin to plane along normal.

    @Override
    public String toString() {
        return "MQ3Plane{" +
                "normal=" + Arrays.toString(m_Normal) +
                ", distance=" + m_Distance +
                '}';
    }

    public static final int SIZE_OF =
            (3*LEDByteAccess.SIZE_OF_FLOAT) +
            LEDByteAccess.SIZE_OF_FLOAT;

}