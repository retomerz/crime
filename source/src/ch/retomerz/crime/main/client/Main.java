package ch.retomerz.crime.main.client;

import ch.retomerz.crime.main.client.jre.java.io.ByteArrayInputStream;
import ch.retomerz.crime.main.client.jre.java.io.ByteArrayOutputStream;
import ch.retomerz.crime.main.client.map.q3.LEDByteAccess;
import ch.retomerz.crime.main.client.map.q3.MQ3MapLoader;
import ch.retomerz.crime.main.client.map.q3.format.MQ3Map;
import ch.retomerz.crime.main.client.map.q3.renderer.RQ3Map;
import ch.retomerz.crime.main.client.map.q3.renderer.RQ3Renderer;
import ch.retomerz.crime.main.client.map.q3.renderer.RQ3Texture;
import ch.retomerz.crime.main.client.util.Base64Decoder;
import ch.retomerz.crime.main.client.zlib.com.jcraft.jzlib.InflaterInputStream;
import com.google.gwt.animation.client.AnimationScheduler;
import com.google.gwt.animation.client.AnimationScheduler.AnimationCallback;
import com.google.gwt.canvas.client.Canvas;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.BodyElement;
import com.google.gwt.dom.client.CanvasElement;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.LoadEvent;
import com.google.gwt.event.dom.client.LoadHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.RootPanel;
import com.googlecode.gwtgl.binding.WebGLRenderingContext;

import java.util.ArrayList;
import java.util.List;

public class Main implements EntryPoint {

  private Canvas m_Canvas;
  private WebGLRenderingContext m_GL;
  private AnimationCallback m_OnFrame;
  private RootPanel m_RootPanel;
  private Image m_ImgLoading;
  private MainServiceAsync m_Service;
  private Timer m_TimerLoadedOld;

  @Override
  public void onModuleLoad() {

    final Document doc = Document.get();
    final BodyElement body = doc.getBody();

    final Style style = body.getStyle();
    style.setPadding(0, Style.Unit.PX);
    style.setMargin(0, Style.Unit.PX);
    style.setBorderWidth(0, Style.Unit.PX);
    style.setProperty("height", "100%");
    style.setBackgroundColor("#FFF");
    style.setColor("#888");

    m_Canvas = Canvas.createIfSupported();

    if (m_Canvas == null) {
      Window.alert("Your browser doesn't support canvas.");
      return;
    }

    m_GL = (WebGLRenderingContext)m_Canvas.getContext("experimental-webgl");
    if (m_GL == null) {
      Window.alert("Your browser doesn't support WebGL.");
      return;
    }
    m_GL.viewport(0, 0, 500, 500); // TODO

    m_ImgLoading = new Image("crime/image/loading.gif");
    m_RootPanel = RootPanel.get();
    m_RootPanel.add(m_ImgLoading);
    m_Service = MainService.App.getInstance();

    loadMap("todo");

    if ( false ) {
      onLoadOld(m_Canvas, m_GL);
    }

  }

  private void startRenderer(RQ3Map _Map) {
    m_RootPanel.remove(m_ImgLoading);
    final RQ3Renderer renderer = new RQ3Renderer(m_Canvas, m_GL, _Map);
    m_Canvas.setCoordinateSpaceHeight(500);
    m_Canvas.setCoordinateSpaceWidth(500);
    m_RootPanel.add(m_Canvas);

    final CanvasElement element = m_Canvas.getCanvasElement();
    final AnimationScheduler scheduler = AnimationScheduler.get();
    m_OnFrame = new AnimationScheduler.AnimationCallback() {

      private double m_LastTimestamp = -1;

      @Override
      public void execute(double timestamp) {
        if (-1 == m_LastTimestamp) {
          m_LastTimestamp = timestamp;
        } else {
          final double frameTime = timestamp - m_LastTimestamp;
          m_LastTimestamp = timestamp;
          renderer.draw(frameTime);
        }
        scheduler.requestAnimationFrame(m_OnFrame, element);
      }

    };
    scheduler.requestAnimationFrame(m_OnFrame, element);
  }

  private void loadMap( String _MapName ) {
    m_Service.getBSP(_MapName, new AsyncCallback<String>() {
      @Override
      public void onFailure(Throwable caught) {
        // TODO
      }

      @Override
      public void onSuccess(String _Result) {
        final MQ3Map formatMap = new MQ3Map();
        MQ3MapLoader.readMap(new LEDByteAccess(Base64Decoder.decode(_Result)), formatMap);
        final RQ3Map map = new RQ3Map(formatMap);
        final List<RQ3Texture> pending = new ArrayList<RQ3Texture>();
        for (RQ3Texture texture : map.m_Textures) {
          if (texture.isRenderable()) {
            pending.add(texture);
          }
        }
        loadTexture(map, pending);
      }
    });
  }

  private void loadTexture(final RQ3Map _Map, final List<RQ3Texture> _Pending) {
    if (_Pending.isEmpty()) {
      startRenderer(_Map);
    } else {
      final RQ3Texture texture = _Pending.remove(0);
      m_Service.getResourceUrl(texture.m_Name, new AsyncCallback<String>() {
        @Override
        public void onFailure(Throwable caught) {
          // TODO
        }

        @Override
        public void onSuccess(String _TextureUrl) {
          final Image img = new Image();
          img.setVisible(false);
          m_RootPanel.add(img);
          img.setUrl(_TextureUrl);
          img.addLoadHandler(new LoadHandler() {
            @Override
            public void onLoad(LoadEvent event) {
              m_RootPanel.remove(img);
              texture.m_Image = img;
              loadTexture(_Map, _Pending);
            }
          });
        }
      });
    }
  }



  private void onLoadOld(final Canvas canvas, final WebGLRenderingContext gl) {

    final boolean[] resourcesLoaded = new boolean[1];
    final Object[] resourcesLoadedByte = new Object[1];
    final Object[] resourcesLoadedImage = new Object[1];
    final Object[] loadedMap = new Object[1];

    MainService.App.getInstance().getByteData("test", new AsyncCallback<byte[]>() {
      @Override
      public void onFailure(Throwable caught) {
        GWT.log("fail load byte", caught);
      }

      @Override
      public void onSuccess(byte[] result) {
        resourcesLoadedByte[0] = result;

        MainService.App.getInstance().getResourceUrl("testimg", new AsyncCallback<String>() {
          @Override
          public void onFailure(Throwable caught) {
            GWT.log("fail load image url", caught);
          }

          @Override
          public void onSuccess(final String _TextureURL) {

            MainService.App.getInstance().getBSP("test.bsp", new AsyncCallback<String>() {
              @Override
              public void onFailure(Throwable caught) {
                GWT.log("fail load bsp", caught);
              }

              @Override
              public void onSuccess(String _Map) {
                loadedMap[0] = _Map;

                final Image img = new Image();
                img.setVisible(false);
                RootPanel.get().add(img);
                img.setUrl(_TextureURL);
                img.addLoadHandler(new LoadHandler() {
                  @Override
                  public void onLoad(LoadEvent event) {
                    RootPanel.get().remove(img);
                    resourcesLoadedImage[0] = img;
                    resourcesLoaded[0] = true;
                  }
                });

              }
            });

          }
        });

      }
    });

    final TestAsyncContext context = new TestAsyncContext(canvas, gl);
    canvas.setCoordinateSpaceHeight(500);
    canvas.setCoordinateSpaceWidth(500);
    RootPanel.get().add(canvas);

    m_TimerLoadedOld = new Timer() {
      boolean m_Initialized;
      @Override
      public void run() {
        if (resourcesLoaded[0]) {
          if (!m_Initialized) {
            m_TimerLoadedOld.cancel();
            m_TimerLoadedOld = null;
            m_Initialized = true;
            //context.init((byte[])resourcesLoadedByte[0], (Image)resourcesLoadedImage[0], decompress((String)loadedMap[0]));
            context.init((byte[])resourcesLoadedByte[0], (Image)resourcesLoadedImage[0], Base64Decoder.decode((String)loadedMap[0]));
            m_RootPanel.remove(m_ImgLoading);
            beginRenderingOld(canvas, context);
          }
        } else {
          GWT.log("resource loading");
        }
      }
    };
    m_TimerLoadedOld.scheduleRepeating(20);
  }

  private void beginRenderingOld(final  Canvas _Canvas, final TestAsyncContext _Context) {
    final CanvasElement element = _Canvas.getCanvasElement();
    final AnimationScheduler scheduler = AnimationScheduler.get();
    m_OnFrame = new AnimationScheduler.AnimationCallback() {
      private double m_LastTimestamp = -1;
      @Override
      public void execute(double timestamp) {
        if (-1 == m_LastTimestamp) {
          m_LastTimestamp = timestamp;
        } else {
          final double frameTime = timestamp - m_LastTimestamp;
          m_LastTimestamp = timestamp;
          _Context.draw(frameTime);
        }
        scheduler.requestAnimationFrame(m_OnFrame, element);
      }
    };
    scheduler.requestAnimationFrame(m_OnFrame, element);
  }

  private static byte[] decompress(String s) {
    byte[] data = Base64Decoder.decode(s);
    s = null;
    final InflaterInputStream in = new InflaterInputStream(new ByteArrayInputStream(data));
    data = null;
    int read;
    final byte[] buf = new byte[1024];
    final ByteArrayOutputStream out = new ByteArrayOutputStream();
    while ( -1 != (read = in.read(buf))) {
      out.write(buf, 0, read);
    }
    return out.toByteArray();
  }

}
