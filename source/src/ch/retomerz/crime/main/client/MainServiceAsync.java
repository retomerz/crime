package ch.retomerz.crime.main.client;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface MainServiceAsync {
    void getResourceUrl(String _Name, AsyncCallback<String> _Async);
    void getByteData(String path, AsyncCallback<byte[]> async);
    void getBSP(String _Name, AsyncCallback<String> _Async);
}
