/**
 * Created by merz on 26.01.14.
 */
package ch.retomerz.crime.main.client;

import ch.retomerz.crime.main.client.util.CubeFactory;
import ch.retomerz.crime.main.client.util.FloatMatrix;
import ch.retomerz.crime.main.client.util.MatrixUtil;
import ch.retomerz.crime.main.client.util.Mesh;
import com.google.gwt.canvas.client.Canvas;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.LoadEvent;
import com.google.gwt.event.dom.client.LoadHandler;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.RootPanel;
import com.googlecode.gwtgl.array.Float32Array;
import com.googlecode.gwtgl.binding.WebGLBuffer;
import com.googlecode.gwtgl.binding.WebGLProgram;
import com.googlecode.gwtgl.binding.WebGLRenderingContext;
import com.googlecode.gwtgl.binding.WebGLShader;
import com.googlecode.gwtgl.binding.WebGLTexture;
import com.googlecode.gwtgl.binding.WebGLUniformLocation;

final class TestContext {

    private final Canvas canvas;
    private final WebGLRenderingContext gl;
    private WebGLProgram shaderProgram;
    private int vertexPositionAttribute;
    private int textureCoordAttribute;
    private WebGLBuffer vertexBuffer;
    private WebGLBuffer vertexTextureCoordBuffer;
    private WebGLUniformLocation projectionMatrixUniform;
    private WebGLUniformLocation textureUniform;
    private WebGLTexture texture;

    private FloatMatrix perspectiveMatrix;
    private FloatMatrix translationMatrix;
    private FloatMatrix rotationMatrix;
    private FloatMatrix resultingMatrix;

    private Mesh cube;

    TestContext(Canvas _canvas, WebGLRenderingContext _gl) {
        canvas = _canvas;
        gl = _gl;
    }

    void init() {
        cube = CubeFactory.createNewInstance(1.0f);
        initParams();
        initShaders();
        initBuffers();
        initTexture();
    }

    private void initParams() {
        //gl.viewport(0, 0, canvas.getOffsetWidth(), canvas.getOffsetHeight());
        gl.viewport(0, 0, 500, 500);
        gl.clearColor(0.2f, 0.0f, 0.0f, 1.0f);
        gl.clearDepth(1.0f);
        gl.enable(WebGLRenderingContext.DEPTH_TEST);
        gl.depthFunc(WebGLRenderingContext.LEQUAL);

        checkErrors();
    }

    private void initShaders() {
        WebGLShader fragmentShader = getShader(WebGLRenderingContext.FRAGMENT_SHADER, Resources.INSTANCE.textureFragmentShader().getText());
        WebGLShader vertexShader = getShader(WebGLRenderingContext.VERTEX_SHADER, Resources.INSTANCE.textureVertexShader().getText());

        // create the ShaderProgram and attach the Shaders
        shaderProgram = gl.createProgram();
        if (shaderProgram == null || gl.getError() != WebGLRenderingContext.NO_ERROR) {
            throw new RuntimeException("program error");
        }

        gl.attachShader(shaderProgram, vertexShader);
        gl.attachShader(shaderProgram, fragmentShader);

        // Bind vertexPosition to attribute 0
        gl.bindAttribLocation(shaderProgram, 0, "vertexPosition");
        // Bind texPosition to attribute 1
        gl.bindAttribLocation(shaderProgram, 1, "texPosition");

        // Link the Shader Program
        gl.linkProgram(shaderProgram);
        if (!gl.getProgramParameterb(shaderProgram, WebGLRenderingContext.LINK_STATUS)) {
            throw new RuntimeException("Could not initialise shaders: " + gl.getProgramInfoLog(shaderProgram));
        }

        // Set the ShaderProgram active
        gl.useProgram(shaderProgram);

        vertexPositionAttribute = gl.getAttribLocation(shaderProgram, "vertexPosition");
        gl.enableVertexAttribArray(vertexPositionAttribute);

        textureCoordAttribute = gl.getAttribLocation(shaderProgram, "texPosition");
        gl.enableVertexAttribArray(textureCoordAttribute);

        // get the position of the projectionMatrix uniform.
        projectionMatrixUniform = gl.getUniformLocation(shaderProgram, "projectionMatrix");

        // get the position of the tex uniform.
        textureUniform = gl.getUniformLocation(shaderProgram, "tex");

        checkErrors();
    }

    private WebGLShader getShader(int type, String source) {
        final WebGLShader shader = gl.createShader(type);
        gl.shaderSource(shader, source);
        gl.compileShader(shader);
        checkErrors();

        if (!gl.getShaderParameterb(shader, WebGLRenderingContext.COMPILE_STATUS)) {
            throw new RuntimeException(gl.getShaderInfoLog(shader));
        }
        return shader;
    }

    private void initBuffers() {
        vertexBuffer = gl.createBuffer();
        gl.bindBuffer(WebGLRenderingContext.ARRAY_BUFFER, vertexBuffer);
        gl.bufferData(WebGLRenderingContext.ARRAY_BUFFER,
                Float32Array.create(cube.getVertices()),
                WebGLRenderingContext.STATIC_DRAW);
        vertexTextureCoordBuffer = gl.createBuffer();
        gl.bindBuffer(WebGLRenderingContext.ARRAY_BUFFER, vertexTextureCoordBuffer);
        gl.bufferData(WebGLRenderingContext.ARRAY_BUFFER, Float32Array.create(cube.getTexCoords()), WebGLRenderingContext.STATIC_DRAW);
        checkErrors();
    }

    private void initTexture() {
        texture = gl.createTexture();
        gl.bindTexture(WebGLRenderingContext.TEXTURE_2D, texture);
        final Image img = getImage(Resources.INSTANCE.texture());
        img.addLoadHandler(new LoadHandler() {
            @Override
            public void onLoad(LoadEvent event) {
                RootPanel.get().remove(img);
                GWT.log("texture image loaded", null);
                gl.bindTexture(WebGLRenderingContext.TEXTURE_2D, texture);
                gl.texImage2D(WebGLRenderingContext.TEXTURE_2D, 0, WebGLRenderingContext.RGBA, WebGLRenderingContext.RGBA, WebGLRenderingContext.UNSIGNED_BYTE, img.getElement());
            }
        });
        checkErrors();
        gl.texParameteri(WebGLRenderingContext.TEXTURE_2D, WebGLRenderingContext.TEXTURE_MAG_FILTER, WebGLRenderingContext.LINEAR);
        gl.texParameteri(WebGLRenderingContext.TEXTURE_2D, WebGLRenderingContext.TEXTURE_MIN_FILTER, WebGLRenderingContext.LINEAR);
        gl.bindTexture(WebGLRenderingContext.TEXTURE_2D, null);
        checkErrors();
    }

    public Image getImage(final ImageResource imageResource) {
        final Image img = new Image();
        img.setVisible(false);
        RootPanel.get().add(img);

        img.setUrl(imageResource.getURL());

        return img;
    }

    void draw() {
        final int angleX = 1;
        final int angleY = 2;
        final int angleZ = 0;
        final float translateZ = -2;

        gl.clear(WebGLRenderingContext.COLOR_BUFFER_BIT | WebGLRenderingContext.DEPTH_BUFFER_BIT);

        gl.vertexAttribPointer(vertexPositionAttribute, 3, WebGLRenderingContext.FLOAT, false, 0, 0);

        // Load the vertex data
        gl.bindBuffer(WebGLRenderingContext.ARRAY_BUFFER, vertexBuffer);
        gl.vertexAttribPointer(vertexPositionAttribute, 3, WebGLRenderingContext.FLOAT, false, 0, 0);

        // Load the texture coordinates data
        gl.bindBuffer(WebGLRenderingContext.ARRAY_BUFFER, vertexTextureCoordBuffer);
        gl.vertexAttribPointer(textureCoordAttribute, 2, WebGLRenderingContext.FLOAT, false, 0, 0);

        perspectiveMatrix = MatrixUtil.createPerspectiveMatrix(45, 1.0f, 0.1f, 100);
        translationMatrix = MatrixUtil.createTranslationMatrix(0, 0, translateZ);
        rotationMatrix = MatrixUtil.createRotationMatrix(angleX, angleY, angleZ);
        resultingMatrix = perspectiveMatrix.multiply(translationMatrix).multiply(rotationMatrix);

        gl.uniformMatrix4fv(projectionMatrixUniform, false, resultingMatrix.getColumnWiseFlatData());

        // Bind the texture to texture unit 0
        gl.activeTexture(WebGLRenderingContext.TEXTURE0);
        gl.bindTexture(WebGLRenderingContext.TEXTURE_2D, texture);

        // Point the uniform sampler to texture unit 0
        gl.uniform1i(textureUniform, 0);
        gl.drawArrays(WebGLRenderingContext.TRIANGLES, 0, 36);
        gl.flush();
        checkErrors();
    }

    private void checkErrors() {
        final int error = gl.getError();
        if (error != WebGLRenderingContext.NO_ERROR) {
            final String message = "WebGL Error: " + error;
            GWT.log(message, null);
            throw new RuntimeException(message);
        }
    }

}
