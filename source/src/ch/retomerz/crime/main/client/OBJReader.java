/**
 * Created by merz on 21.04.14.
 */
package ch.retomerz.crime.main.client;

import ch.retomerz.crime.main.client.model.Face1;
import ch.retomerz.crime.main.client.model.Face2;
import ch.retomerz.crime.main.client.model.Face3;
import ch.retomerz.crime.main.client.model.Faces;
import ch.retomerz.crime.main.client.model.OBJFaceParser;
import ch.retomerz.crime.main.client.model.Vector2f;
import ch.retomerz.crime.main.client.model.Vector3f;
import ch.retomerz.crime.main.client.util.CollectionUtil;
import ch.retomerz.crime.main.client.util.Mesh;
import ch.retomerz.crime.main.client.util.New;

import java.util.List;

public final class OBJReader {

    private OBJReader() {
    }

    public static Mesh load(String model) {

        final List<Vector3f> vertex = New.arrayList();
        final List<Vector2f> texture = New.arrayList();
        final List<Vector3f> normal = New.arrayList();

        String name = null;
        String usemtl = null;
        OBJFaceParser<? extends Face1> faceParser = null;
        Faces<? extends Face1> faces = null;

        String[] sp;

        model = model.replace("\r\n", "\n").replace("\r", "\n");
        final String[] lines = model.split("\n");
        for (String line : lines) {

            //noinspection StatementWithEmptyBody
            if (line.isEmpty()) {
                // skip

            } else if (line.startsWith("g ")) {
                if (null != name) {
                    throw new IllegalStateException("Illegal format");
                }
                name = line.substring(2, line.length());

            } else if (line.startsWith("v ")) {
                sp = line.split(" ");
                vertex.add(Vector3f.of(
                        Float.parseFloat(sp[1]),
                        Float.parseFloat(sp[2]),
                        Float.parseFloat(sp[3])
                ));

            } else if (line.startsWith("vt ")) {
                sp = line.split(" ");
                texture.add(Vector2f.of(
                        Float.parseFloat(sp[1]),
                        Float.parseFloat(sp[2])
                ));

            } else if (line.startsWith("vn ")) {
                sp = line.split(" ");
                normal.add(Vector3f.of(
                        Float.parseFloat(sp[1]),
                        Float.parseFloat(sp[2]),
                        Float.parseFloat(sp[3])
                ));

            } else if (line.startsWith("f ")) {
                sp = line.split(" ");
                if (null == faceParser) {
                    faceParser = OBJFaceParser.create(sp[1]);
                }
                final Face1 face = faceParser.parse(sp, 1, sp.length);
                if (null == faces) {
                    faces = Faces.create(face);
                }
                faces.unsafeAdd(face);

            } else if (line.startsWith("usemtl ")) {
                if (null != usemtl) {
                    throw new IllegalStateException("Illegal format");
                }
                usemtl = line.substring("usemtl ".length(), line.length());

            } else if (line.startsWith("#")) {
                System.out.println("OBJ comment: " + line);
            } else {
                System.out.println("Unknwon line");
            }
        }

        if (null == faces) {
            throw new IllegalStateException("No faces");
        }
        faces.setName(name);
        faces.setUsemtl(usemtl);
        return create(faces, vertex, texture, normal);
    }

    private static Mesh create(Faces<? extends Face1> _Faces, List<Vector3f> _Vertex, List<Vector2f> m_Texture, List<Vector3f> _Normal) {

        final List<Float> vs = New.arrayList();
        final List<Float> ts = New.arrayList();
        final List<Float> ns = New.arrayList();

        for (Face1 face : _Faces.all()) {
            switch (_Faces.getType()) {
                case Vertex_TextureCoordinate_Normal:
                    for (int normalIndex : ((Face3)face).m_Normal) {
                        Vector3f.add(_Normal.get(normalIndex-1), ns);
                    }
                    // fall
                case Vertex_TextureCoordinate:
                    for (int textureIndex : ((Face2)face).m_Texture) {
                        Vector2f.add(m_Texture.get(textureIndex-1), ts);
                    }
                    // fall
                case Vertex:
                    for (int vertexIndex : face.m_Vertex) {
                        Vector3f.add(_Vertex.get(vertexIndex-1), vs);
                    }
                    break;
                default:
                    throw new IllegalArgumentException("Not supported " + _Faces.getType());
            }
        }

        final Mesh m = new Mesh(vs.size(), ts.size(), ns.size());
        m.verticesArray = CollectionUtil.floatToArray(vs);
        m.texCoordsArray = CollectionUtil.floatToArray(ts);
        m.vertexNormalsArray = CollectionUtil.floatToArray(ns);
        return m;
    }

}
