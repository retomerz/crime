/**
 * Created by merz on 26.01.14.
 */
package ch.retomerz.crime.main.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.resources.client.TextResource;

public interface Resources extends ClientBundle {

    public static final Resources INSTANCE = GWT.create(Resources.class);

    //@ClientBundle.Source(value = {"texture-fragment-shader.txt"})
    @ClientBundle.Source(value = {"texture-fragment-shader-new.txt"})
    TextResource textureFragmentShader();

    //@ClientBundle.Source(value = {"texture-vertex-shader.txt"})
    @ClientBundle.Source(value = {"texture-vertex-shader-new.txt"})
    TextResource textureVertexShader();

    @ClientBundle.Source(value = {"texture2.png"})
    ImageResource texture();
}
