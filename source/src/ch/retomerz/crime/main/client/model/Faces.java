/**
 * Created by merz on 22.04.14.
 */
package ch.retomerz.crime.main.client.model;

import java.util.ArrayList;
import java.util.List;

public final class Faces<F extends Face1> {

    private final FaceType m_Type;
    private String m_Name;
    private String m_Usemtl;
    private final List<F> m_Face = new ArrayList<F>();

    private Faces(FaceType _Type) {
        m_Type = _Type;
    }

    public FaceType getType() {
        return m_Type;
    }

    public boolean hasName() {
        return null != m_Name;
    }

    public void setName(String _Value) {
        if (hasName()) {
            throw new IllegalStateException("Multiple name not supported");
        }
        m_Name = _Value;
    }

    public boolean hasUsemtl() {
        return null != m_Usemtl;
    }

    public void setUsemtl(String _Value) {
        if (hasUsemtl()) {
            throw new IllegalStateException("Multiple usemtl not supported");
        }
        m_Usemtl = _Value;
    }

    @SuppressWarnings("unchecked")
    public void unsafeAdd(Face1 _Face) {
        m_Face.add((F)_Face);
    }

    public void add(F _Face) {
        m_Face.add(_Face);
    }

    public List<F> all() {
        return m_Face;
    }

    @SuppressWarnings("Convert2Diamond") // JS compiler
    public static <F extends Face1> Faces<F> create(F _Face) {
        return new Faces<F>(FaceType.of(_Face));
    }

}