/**
 * Created by merz on 22.04.14.
 */
package ch.retomerz.crime.main.client.model;

import java.util.Collection;

public final class Vector2f {

    public final float m_U;
    public final float m_V;

    private Vector2f(float _U, float _V) {
        m_U = _U;
        m_V = _V;
    }

    @Override
    public String toString() {
        return "Vector2f{" +
                "u=" + m_U +
                ", v=" + m_V +
                '}';
    }

    public static Vector2f of(float _U, float _V) {
        return new Vector2f(_U, _V);
    }

    public static void add(Vector2f _Vector, Collection<Float> _Collection) {
        _Collection.add(_Vector.m_U);
        _Collection.add(_Vector.m_V);
    }

}