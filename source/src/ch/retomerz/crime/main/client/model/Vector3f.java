/**
 * Created by merz on 22.04.14.
 */
package ch.retomerz.crime.main.client.model;

import java.util.Collection;

public final class Vector3f {

    public final float m_X;
    public final float m_Y;
    public final float m_Z;

    private Vector3f(float _X, float _Y, float _Z) {
        m_X = _X;
        m_Y = _Y;
        m_Z = _Z;
    }

    @Override
    public String toString() {
        return "Vector3f{" +
                "x=" + m_X +
                ", y=" + m_Y +
                ", z=" + m_Z +
                '}';
    }

    public static Vector3f of(float _X, float _Y, float _Z) {
        return new Vector3f(_X, _Y, _Z);
    }

    public static void add(Vector3f _Vector, Collection<Float> _Collection) {
        _Collection.add(_Vector.m_X);
        _Collection.add(_Vector.m_Y);
        _Collection.add(_Vector.m_Z);
    }

}