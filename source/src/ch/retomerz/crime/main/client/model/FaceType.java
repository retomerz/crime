/**
 * Created by merz on 22.04.14.
 */
package ch.retomerz.crime.main.client.model;

public enum FaceType {

    Vertex(1, Face1.class),
    Vertex_TextureCoordinate(2, Face2.class),
    Vertex_TextureCoordinate_Normal(3, Face3.class);

    public final int m_ElementCount;
    public final Class<? extends Face1> m_Class;

    FaceType(int _ElementCount, Class<? extends Face1> _Class) {
        m_ElementCount = _ElementCount;
        m_Class = _Class;
    }

    public static <F extends Face1> FaceType of(F _Face) {
        final Class clazz = _Face.getClass();
        for (FaceType type : values()) {
            if (type.m_Class == clazz) {
                return type;
            }
        }
        throw new IllegalArgumentException("Not supported " + clazz);
    }

    public static FaceType of(int _ElementCount) {
        for (FaceType type : values()) {
            if (type.m_ElementCount == _ElementCount) {
                return type;
            }
        }
        throw new IllegalArgumentException("Not supported " + _ElementCount);
    }

}