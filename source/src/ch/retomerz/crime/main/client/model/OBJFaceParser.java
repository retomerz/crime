/**
 * Created by merz on 23.04.14.
 */
package ch.retomerz.crime.main.client.model;

import ch.retomerz.crime.main.client.util.CollectionUtil;
import ch.retomerz.crime.main.client.util.New;

import java.util.List;

public final class OBJFaceParser<F extends Face1> {

    private final FaceType m_Type;
    public final List<Integer> m_Vertex;
    public final List<Integer> m_Texture;
    public final List<Integer> m_Normal;

    private OBJFaceParser(FaceType _Type) {
        m_Type = _Type;
        m_Vertex = New.arrayList();
        m_Texture = New.arrayList();
        m_Normal = New.arrayList();
    }

    @SuppressWarnings("unchecked")
    public F parse(String[] _FaceElements, int _Offset, int _End) {
        for (int i = _Offset; i < _End; i++) {
            parseElement(_FaceElements[i]);
        }
        return createFace();
    }

    private void parseElement(String _FaceElement) {
        switch (m_Type) {
            case Vertex:
                parse1(_FaceElement);
                break;
            case Vertex_TextureCoordinate:
                parse2(_FaceElement);
                break;
            case Vertex_TextureCoordinate_Normal:
                parse3(_FaceElement);
                break;
            default:
                throw new IllegalArgumentException("Not supported " + m_Type);
        }
    }

    @SuppressWarnings("unchecked")
    private F createFace() {
        final F ret;
        switch (m_Type) {
            case Vertex:
                ret = (F)Face1.of(CollectionUtil.integerToArray(m_Vertex));
                break;
            case Vertex_TextureCoordinate:
                ret = (F)Face2.of(CollectionUtil.integerToArray(m_Vertex), CollectionUtil.integerToArray(m_Texture));
                break;
            case Vertex_TextureCoordinate_Normal:
                ret = (F)Face3.of(CollectionUtil.integerToArray(m_Vertex), CollectionUtil.integerToArray(m_Texture), CollectionUtil.integerToArray(m_Normal));
                break;
            default:
                throw new IllegalArgumentException("Not supported " + m_Type);
        }
        m_Vertex.clear();
        m_Texture.clear();
        m_Normal.clear();
        return ret;
    }

    private void parse1(String _FaceElement) {
        m_Vertex.add(Integer.parseInt(_FaceElement));
    }

    private void parse2(String _FaceElement) {
        final int pos = _FaceElement.indexOf("/");
        m_Vertex.add(Integer.parseInt(_FaceElement.substring(0, pos)));
        m_Texture.add(Integer.parseInt(_FaceElement.substring(pos + 1)));
    }

    private void parse3(String _FaceElement) {
        final int pos = _FaceElement.indexOf("/");
        final int pos2 = _FaceElement.indexOf("/", pos + 1);
        m_Vertex.add(Integer.parseInt(_FaceElement.substring(0, pos)));
        m_Texture.add(Integer.parseInt(_FaceElement.substring(pos + 1, pos2)));
        m_Normal.add(Integer.parseInt(_FaceElement.substring(pos2 + 1)));
    }

    @SuppressWarnings("Convert2Diamond") // JS compiler
    public static OBJFaceParser<? extends Face1> create(String _Face) {
        final int count = count(_Face, "/");
        final FaceType type = FaceType.of(count+1);
        switch (type) {
            case Vertex:
                return new OBJFaceParser<Face1>(type);
            case Vertex_TextureCoordinate:
                return new OBJFaceParser<Face2>(type);
            case Vertex_TextureCoordinate_Normal:
                return new OBJFaceParser<Face3>(type);
            default:
                throw new IllegalArgumentException("Not supported " + type);
        }
    }

    private static int count(String s, String _char) {
        int count = 0;
        int pos;
        int off = 0;
        while (-1 != (pos = s.indexOf(_char, off))) {
            count++;
            off = pos+1;
        }
        return count;
    }

}