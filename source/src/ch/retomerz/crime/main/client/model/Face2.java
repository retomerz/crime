/**
 * Created by merz on 22.04.14.
 */
package ch.retomerz.crime.main.client.model;

public class Face2 extends Face1 {

    public final int[] m_Texture;

    Face2(int[] _Vertex, int[] _Texture) {
        super(_Vertex);
        m_Texture = _Texture;
    }

    public static Face2 of(int[] _Vertex, int[] _Texture) {
        return new Face2(_Vertex, _Texture);
    }

}