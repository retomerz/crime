/**
 * Created by merz on 22.04.14.
 */
package ch.retomerz.crime.main.client.model;

public class Face1 {

    public final int[] m_Vertex;

    Face1(int[] _Vertex) {
        m_Vertex = _Vertex;
    }

    public static Face1 of(int[] _Vertex) {
        return new Face1(_Vertex);
    }

}