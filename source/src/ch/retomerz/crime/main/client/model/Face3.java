/**
 * Created by merz on 22.04.14.
 */
package ch.retomerz.crime.main.client.model;

public final class Face3 extends Face2 {

    public final int[] m_Normal;

    Face3(int[] _Vertex, int[] _Texture, int[] _Normal) {
        super(_Vertex, _Texture);
        m_Normal = _Normal;
    }

    public static Face3 of(int[] _Vertex, int[] _Texture, int[] _Normal) {
        return new Face3(_Vertex, _Texture, _Normal);
    }

}