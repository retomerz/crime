/**
 * Created by merz on 01.05.14.
 */
package ch.retomerz.crime.main.client.map.q3.format;

public final class MQ3VisData {
    public int m_NbClusters; // The number of clusters
    public int m_BytesPerCluster; // Bytes (8 bits) in the cluster's bitset
    public /* unsigned char* */ char[]  m_Buffer; // Array of bytes holding the cluster vis
}