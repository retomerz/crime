/**
 * Created by merz on 19.05.14.
 */
package ch.retomerz.crime.main.client.map.q3.renderer;

import ch.retomerz.crime.main.client.map.q3.format.MQ3Plane;

public final class RQ3TraceResult {

  public boolean m_AllSolid;
  private boolean m_StartSolid;
  public float m_Fraction;
  public float[] m_EndPos;
  public MQ3Plane m_Plane;
  public boolean m_StartsOut;

  public RQ3TraceResult() {
    super();
  }

  public void init(final float[] end) {
    m_AllSolid = false;
    m_StartSolid = false;
    m_Fraction = 1.0f;
    m_EndPos = end;
    m_Plane = null;

    m_StartsOut = false; // TODO or null ??
  }

}