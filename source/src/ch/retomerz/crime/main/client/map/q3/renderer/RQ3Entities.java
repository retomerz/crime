/**
 * Created by merz on 14.05.14.
 */
package ch.retomerz.crime.main.client.map.q3.renderer;

import ch.retomerz.crime.main.client.map.q3.format.MQ3Entity;
import ch.retomerz.crime.main.client.map.q3.renderer.entity.RQ3EntityInfoPlayerDeathmatch;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class RQ3Entities {

  private List<RQ3EntityInfoPlayerDeathmatch> m_InfoPlayerDeathmatch;

  private RQ3Entities() {
  }

  private void init(MQ3Entity _Entity) {
    final String s = new String(_Entity.m_Buffer);
    final String[] blocks = s.split("\\{");
    int pos;
    Map<String, String> map;

    for (String block : blocks) {
      if (block.contains("classname")) {
        pos = block.indexOf("}");
        if (-1 == pos) {
          throw new IllegalArgumentException("Invalid entities: " + s);
        }
        block = block.substring(0, pos);
        map = parseBlock(block);
        parseBlock(map);
      }
    }
  }

  private void parseBlock(Map<String, String> _Map) {
    final String classname = _Map.get("classname");
    if ("info_player_deathmatch".equals(classname)) {
      if (null == m_InfoPlayerDeathmatch) {
        m_InfoPlayerDeathmatch = new ArrayList<RQ3EntityInfoPlayerDeathmatch>();
      }
      m_InfoPlayerDeathmatch.add(RQ3EntityInfoPlayerDeathmatch.of(_Map));
    }
  }

  public List<RQ3EntityInfoPlayerDeathmatch> getInfoPlayerDeathmatch() {
    return m_InfoPlayerDeathmatch;
  }

  public static RQ3Entities of(MQ3Entity _Entity) {
    final RQ3Entities ret = new RQ3Entities();
    ret.init(_Entity);
    return ret;
  }

  private static Map<String, String> parseBlock(String _Block) {
    final Map<String, String> ret = new HashMap<String, String>();
    final String[] lines = _Block.replace("\r\n", "\n").replace("\r", "\n").split("\n");
    int posKeyEnd;
    int posValueStart;
    String key;
    String value;

    for (String line : lines) {
      line = line.trim();
      if (line.startsWith("\"") && line.endsWith("\"")) {
        posKeyEnd = line.indexOf("\"", 1);
        if (-1 != posKeyEnd) {
          posValueStart = line.indexOf("\"", posKeyEnd+1);
          if (-1 != posValueStart) {
            key = line.substring(1, posKeyEnd);
            value = line.substring(posValueStart+1, line.length()-1);
            ret.put(key, value);
          } else {
            throw new IllegalArgumentException("Invalid block (position value start): " + _Block);
          }
        } else {
          throw new IllegalArgumentException("Invalid block (position key end): " + _Block);
        }
      } else if (line.isEmpty()) {
        // ; skip empty lines
      } else {
        throw new IllegalArgumentException("Invalid block (line brackets): " + _Block);
      }
    }
    return ret;
  }

}