/**
 * Created by merz on 01.05.14.
 */
package ch.retomerz.crime.main.client.map.q3.format;

import java.util.ArrayList;
import java.util.List;

public class MQ3Map {

    public MQ3Header m_Header;
    public MQ3Entity m_Entity;
    public final List<MQ3Texture> m_Textures;
    public final List<MQ3Plane> m_Planes;
    public final List<MQ3Node> m_Nodes;
    public final List<MQ3Leaf> m_Leaves;
    public final List<MQ3LeafFace> m_LeafFaces;
    public final List<MQ3LeafBrush> m_LeafBrushes;
    public final List<MQ3Model> m_Models;
    public final List<MQ3Brush> m_Brushes;
    public final List<MQ3BrushSide> m_BrushSides;
    public final List<MQ3Vertex> m_Vertices;
    public final List<MQ3MeshVert> m_MeshVertices;
    public final List<MQ3Effect> m_Effects;
    public final List<MQ3Face> m_Faces;
    //public final List<MQ3LightMap> m_LightMaps;
    public MQ3LightMap m_LightMap;
    public final List<MQ3LightVol> m_LightVols;
    public MQ3VisData m_VisData;

    @SuppressWarnings("Convert2Diamond") // JS compiler
    public MQ3Map() {
        m_Textures = new ArrayList<MQ3Texture>();
        m_Planes = new ArrayList<MQ3Plane>();
        m_Nodes = new ArrayList<MQ3Node>();
        m_Leaves = new ArrayList<MQ3Leaf>();
        m_LeafFaces = new ArrayList<MQ3LeafFace>();
        m_LeafBrushes = new ArrayList<MQ3LeafBrush>();
        m_Models = new ArrayList<MQ3Model>();
        m_Brushes = new ArrayList<MQ3Brush>();
        m_BrushSides = new ArrayList<MQ3BrushSide>();
        m_Vertices = new ArrayList<MQ3Vertex>();
        m_MeshVertices = new ArrayList<MQ3MeshVert>();
        m_Effects = new ArrayList<MQ3Effect>();
        m_Faces = new ArrayList<MQ3Face>();
        //m_LightMaps = new ArrayList<MQ3LightMap>();
        m_LightVols = new ArrayList<MQ3LightVol>();
    }

}