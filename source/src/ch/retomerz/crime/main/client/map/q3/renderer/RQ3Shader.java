/**
 * Created by merz on 24.05.14.
 */
package ch.retomerz.crime.main.client.map.q3.renderer;

import com.googlecode.gwtgl.binding.WebGLRenderingContext;

import java.util.ArrayList;
import java.util.List;

public final class RQ3Shader {

  public int m_Cull = WebGLRenderingContext.FRONT;
  public boolean m_Blend;
  public int m_Sort = 3;
  public final List<RQ3ShaderStage> m_Stages;

  public RQ3Shader(RQ3ShaderStage stage) {
    m_Stages = new ArrayList<RQ3ShaderStage>();
    m_Stages.add(stage);
  }

}
