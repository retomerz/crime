/**
 * Created by merz on 01.05.14.
 */
package ch.retomerz.crime.main.client.map.q3.format;

public final class MQ3Entity {
    public int m_Size; // Size of the description.
    public char[] m_Buffer; // Entity descriptions, stored as a string.
}