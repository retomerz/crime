/**
 * Created by merz on 01.05.14.
 */
package ch.retomerz.crime.main.client.map.q3.format;

import ch.retomerz.crime.main.server.temp.map.q3.LEDByteAccess;

public final class MQ3LeafBrush {
    public int m_BrushIndex; // Brush index.

    @Override
    public String toString() {
        return "MQ3LeafBrush{" +
                "brushIndex=" + m_BrushIndex +
                '}';
    }

    public static final int SIZE_OF = LEDByteAccess.SIZE_OF_INT;
}