/**
 * Created by merz on 18.05.14.
 */
package ch.retomerz.crime.main.client.map.q3.renderer;

import ch.retomerz.crime.main.client.TestMQ3;
import ch.retomerz.crime.main.client.map.q3.renderer.entity.RQ3EntityInfoPlayerDeathmatch;
import ch.retomerz.crime.main.client.math.Vector3f;

import java.util.ArrayList;
import java.util.List;

public final class RQ3MovementOld {

  /**
   * \game\bg_local.h
   */
  private static final float OVERCLIP = 1.001f;

  /**
   * \game\bg_slidemove.c
   */
  private static final int MAX_CLIP_PLANES = 5;

  private static final float q3movement_playerRadius = 10.0f;
  private static final float q3movement_scale = 50f;
  private static final float q3movement_accelerate = 10.0f;
  private static final float q3movement_airaccelerate = 0.1f;
  private static final float q3movement_stopspeed = 100.0f;
  private static final float q3movement_friction = 6.0f;
  private static final float q3movement_stepsize = 18f;
  private static final float q3movement_gravity = 20.0f;
  private static final float q3movement_jumpvelocity = 50f;

  private float q3movement_frameTime = 0.30f;

  private final TestMQ3 m_Map;
  private float[] m_Velocity;
  public final float[] m_Position;
  public float m_AngleX;
  public float m_AngleZ;
  private boolean m_OnGround;
  private final RQ3TraceResult m_GroundTrace;

  public RQ3MovementOld(TestMQ3 _Map) {
    m_Map = _Map;
    m_Velocity = new float[3];
    m_Position = new float[3];
    m_GroundTrace = new RQ3TraceResult();
  }

  public boolean jump() {
    if(!m_OnGround) { return false; }

    m_OnGround = false;
    m_Velocity[2] = q3movement_jumpvelocity;

    //Make sure that the player isn't stuck in the ground
    final float groundDist = Vector3f.dot(m_Position, m_GroundTrace.m_Plane.m_Normal ) - m_GroundTrace.m_Plane.m_Distance - q3movement_playerRadius;
    final float[] vec = new float[3];
    Vector3f.scale(m_GroundTrace.m_Plane.m_Normal, groundDist + 5, vec);
    Vector3f.add(m_Position, vec);
    return true;
  }

  public void move(float[] dir, double frameTime) {
    q3movement_frameTime = (float)frameTime*0.0075f;

    groundCheck();

    Vector3f.normalize(dir);

    if (m_OnGround) {
      walkMove(dir);
    } else {
      airMove(dir);
    }
  }

  public void look(float x, float y) {
    lookImpl(x, y);
  }

  private void lookImpl(float xDelta, float yDelta) {
    m_AngleZ += xDelta*0.0025;
    while (m_AngleZ < 0)
      m_AngleZ += Math.PI*2;
    while (m_AngleZ >= Math.PI*2)
      m_AngleZ -= Math.PI*2;

    m_AngleX += yDelta*0.0025;
    while (m_AngleX < -Math.PI*0.5)
      m_AngleX += (float)(-Math.PI*0.5); // TODO MERZ check original code: xAngle = -Math.PI*0.5;
    while (m_AngleX > Math.PI*0.5)
      m_AngleX -= (float)(Math.PI*0.5); // TODO MERZ check original code: xAngle = Math.PI*0.5;
  }

  public void spawn(RQ3Entities _Entities) {
    if (null == _Entities.getInfoPlayerDeathmatch()) {
      return;
    }
    final RQ3EntityInfoPlayerDeathmatch ipd = _Entities.getInfoPlayerDeathmatch().get(0);
    m_Position[0] = ipd.m_Dir[0];
    m_Position[1] = ipd.m_Dir[1];
    m_Position[2] = ipd.m_Dir[2] + 30; // TODO 30
  }

  private void walkMove(float[] dir) {
    applyFriction();

    float speed = Vector3f.length(dir) * q3movement_scale;

    this.accelerate(dir, speed, q3movement_accelerate);

    m_Velocity = this.clipVelocity(m_Velocity, m_GroundTrace.m_Plane.m_Normal);

    // TODO MERZ check code original: if(!this.velocity[0] && !this.velocity[1]) { return; }
    if(0 == m_Velocity[0] && 0 == m_Velocity[1]) { return; }

    stepSlideMove( false );
  }

  private void airMove(float[] dir) {
    float speed = Vector3f.length(dir) * q3movement_scale;

    this.accelerate(dir, speed, q3movement_airaccelerate);

    this.stepSlideMove( true );
  }

  /**
   * Slide off of the impacting surface
   * \game\bg_pmove.c
   * void PM_ClipVelocity( vec3_t in, vec3_t normal, vec3_t out, float overbounce )
   *
   * @param velIn NotNull
   * @param normal NotNull
   * @return NotNull
   */
  private float[] clipVelocity(float[] velIn, float[] normal) {
    float backoff = Vector3f.dot(velIn, normal);

    if ( backoff < 0 ) {
      backoff *= OVERCLIP;
    } else {
      backoff /= OVERCLIP;
    }

    final float[] change = new float[3];
    Vector3f.scale(normal, backoff, change);
    Vector3f.subtract(velIn, change, change);
    return change;
  }

  private void applyFriction() {
    if(!m_OnGround) { return; }

    float speed = Vector3f.length(m_Velocity);

    float drop = 0;

    float control = speed < q3movement_stopspeed ? q3movement_stopspeed : speed;
    drop += control*q3movement_friction*q3movement_frameTime;

    float newSpeed = speed - drop;
    if (newSpeed < 0) {
      newSpeed = 0;
    }
    if(speed != 0) {
      newSpeed /= speed;
      Vector3f.scale(m_Velocity, newSpeed);
    } else {
      Vector3f.zero(m_Velocity);
    }
  }

  /**
   * Handles user intended acceleration
   * \game\bg_pmove.c
   * static void PM_Accelerate( vec3_t wishdir, float wishspeed, float accel )
   *
   * @param _WishDir NotNull
   * @param _WishSpeed ..
   * @param _Accel ..
   */
  private void accelerate(final float[] _WishDir, final float _WishSpeed, final float _Accel) {
    float currentSpeed = Vector3f.dot(m_Velocity, _WishDir);
    float addSpeed = _WishSpeed - currentSpeed;
    if (addSpeed <= 0) {
      return;
    }

    float accelSpeed = _Accel*q3movement_frameTime*_WishSpeed;
    if (accelSpeed > addSpeed) {
      accelSpeed = addSpeed;
    }

    m_Velocity[0] += accelSpeed*_WishDir[0];
    m_Velocity[1] += accelSpeed*_WishDir[1];
    m_Velocity[2] += accelSpeed*_WishDir[2];
  }

  private void groundCheck() {
    float[] checkPoint = new float[] {m_Position[0], m_Position[1], (float)(m_Position[2] - q3movement_playerRadius - 0.25)};

    m_Map.trace(m_GroundTrace, m_Position, checkPoint, q3movement_playerRadius);

    if(m_GroundTrace.m_Fraction == 1.0) { // falling
      m_OnGround = false;
      return;
    }

    if ( m_Velocity[2] > 0 && Vector3f.dot(m_Velocity, m_GroundTrace.m_Plane.m_Normal) > 10 ) { // jumping
      m_OnGround = false;
      return;
    }

    if(m_GroundTrace.m_Plane.m_Normal[2] < 0.7) { // steep slope
      m_OnGround = false;
      return;
    }

    m_OnGround = true;
  }

  /**
   *
   * @param gravity ..
   */
  private void stepSlideMove(boolean gravity) {
    float[] start_o = new float[3];
    Vector3f.set(m_Position, start_o);
    float[] start_v = new float[3];
    Vector3f.set(m_Velocity, start_v);

    // TODO MERZ check source original code : if ( slideMove( gravity ) == 0 ) { return; }
    if ( !slideMove( gravity ) ) { return; } // we got exactly where we wanted to go first try

    float[] down = new float[3];
    Vector3f.set(start_o, down);
    down[2] -= q3movement_stepsize;
    RQ3TraceResult trace = new RQ3TraceResult();
    m_Map.trace(trace, start_o, down, q3movement_playerRadius);

    float[] up = new float[]{0,0,1};

    // never step up when you still have up velocity
    if ( m_Velocity[2] > 0 && (trace.m_Fraction == 1.0 || Vector3f.dot(trace.m_Plane.m_Normal, up) < 0.7)) { return; }

    float[] down_o = new float[3];
    Vector3f.set(m_Position, down_o);
    float[] down_v = new float[3];
    Vector3f.set(this.m_Velocity, down_v);

    Vector3f.set(start_o, up);
    up[2] += q3movement_stepsize;

    // test the player position if they were a stepheight higher
    trace = new RQ3TraceResult();
    m_Map.trace(trace, start_o, up, q3movement_playerRadius);
    if ( trace.m_AllSolid ) { return; } // can't step up

    float stepSize = trace.m_EndPos[2] - start_o[2];
    // try slidemove from this position
    Vector3f.set(trace.m_EndPos, m_Position);
    Vector3f.set(start_v, m_Velocity);

    this.slideMove( gravity );

    // push down the final amount
    Vector3f.set(m_Position, down);
    down[2] -= stepSize;
    trace = new RQ3TraceResult();
    m_Map.trace(trace, m_Position, down, q3movement_playerRadius);
    if ( !trace.m_AllSolid ) {
      Vector3f.set(trace.m_EndPos, m_Position);
    }
    if ( trace.m_Fraction < 1.0 ) {
      m_Velocity = clipVelocity(m_Velocity, trace.m_Plane.m_Normal);
    }
  }

  /**
   * Returns true if the velocity was clipped in some way
   * \game\bg_slidemove.c
   * qboolean	PM_SlideMove( qboolean gravity )
   *
   * @param gravity ..
   * @return ..
   */
  private boolean slideMove(boolean gravity) {
    int bumpcount = 0;
    int numbumps = 4;
    int i;
    List<float[]> planes = new ArrayList<float[]>();
    float[] endVelocity = new float[3];

    if ( gravity ) {
      Vector3f.set(m_Velocity, endVelocity );
      endVelocity[2] -= q3movement_gravity * q3movement_frameTime;
      m_Velocity[2] = ( m_Velocity[2] + endVelocity[2] ) * 0.5f;

      if ( null != m_GroundTrace && null != m_GroundTrace.m_Plane ) {
        // slide along the ground plane
        m_Velocity = this.clipVelocity(m_Velocity, m_GroundTrace.m_Plane.m_Normal);
      }
    }

    // never turn against the ground plane
    if ( null != m_GroundTrace && null != m_GroundTrace.m_Plane ) {
      float[] vec = new float[3];
      Vector3f.set(m_GroundTrace.m_Plane.m_Normal, vec);
      planes.add(vec);
    }

    // never turn against original velocity
    float[] vec = new float[3];
    Vector3f.normalize(m_Velocity, vec);
    planes.add(vec);

    float time_left = q3movement_frameTime;
    float[] end = new float[3];
    for(bumpcount=0; bumpcount < numbumps; ++bumpcount) {

      // calculate position we are trying to move to
      vec = new float[3];
      Vector3f.scale(m_Velocity, time_left, vec);
      Vector3f.add(m_Position, vec, end);

      // see if we can make it there
      RQ3TraceResult trace = new RQ3TraceResult();
      m_Map.trace(trace, m_Position, end, q3movement_playerRadius);

      if (trace.m_AllSolid) {
        // entity is completely trapped in another solid
        m_Velocity[2] = 0;   // don't build up falling damage, but allow sideways acceleration
        return true;
      }

      if (trace.m_Fraction > 0) {
        // actually covered some distance
        Vector3f.set(trace.m_EndPos, m_Position);
      }

      if (trace.m_Fraction == 1) {
        break;     // moved the entire distance
      }

      time_left -= time_left * trace.m_Fraction;

      if (planes.size() >= MAX_CLIP_PLANES) {
        // this shouldn't really happen
        Vector3f.zero(m_Velocity);
        return true;
      }

      //
      // if this is the same plane we hit before, nudge velocity
      // out along it, which fixes some epsilon issues with
      // non-axial planes
      //
      for (i = 0; i < planes.size(); i++) {
        if (Vector3f.dot(trace.m_Plane.m_Normal, planes.get(i)) > 0.99 ) {
          Vector3f.add(trace.m_Plane.m_Normal, m_Velocity, m_Velocity);
          break;
        }
      }
      if ( i < planes.size() ) {
        continue;
      }
      vec = new float[3];
      Vector3f.set(trace.m_Plane.m_Normal, vec);
      planes.add(vec);

      //
      // modify velocity so it parallels all of the clip planes
      //

      // find a plane that it enters
      for(i = 0; i < planes.size(); ++i) {
        float into = Vector3f.dot(m_Velocity, planes.get(i));
        if ( into >= 0.1 ) {
          continue; // move doesn't interact with the plane
        }

        // slide along the plane
        float[] clipVelocity = clipVelocity(m_Velocity, planes.get(i));
        float[] endClipVelocity = clipVelocity(endVelocity, planes.get(i));

        // see if there is a second plane that the new move enters
        for (int j = 0; j < planes.size(); j++) {
          if (j == i) {
            continue;
          }
          if (Vector3f.dot(clipVelocity, planes.get(j)) >= 0.1 ) {
            continue; // move doesn't interact with the plane
          }

          // try clipping the move to the plane
          clipVelocity = clipVelocity(clipVelocity, planes.get(j));
          endClipVelocity = clipVelocity(endClipVelocity, planes.get(j));

          // see if it goes back into the first clip plane
          if (Vector3f.dot(clipVelocity, planes.get(i)) >= 0 ) {
            continue;
          }

          // slide the original velocity along the crease
          float[] dir = new float[3];
          Vector3f.cross(planes.get(i), planes.get(j), dir);
          Vector3f.normalize(dir);
          float d = Vector3f.dot(dir, m_Velocity);
          Vector3f.scale(dir, d, clipVelocity);

          Vector3f.cross(planes.get(i), planes.get(j), dir);
          Vector3f.normalize(dir);
          d = Vector3f.dot(dir, endVelocity);
          Vector3f.scale(dir, d, endClipVelocity);

          // see if there is a third plane the the new move enters
          for(int k = 0; k < planes.size(); ++k) {
            if (k == i || k == j) {
              continue;
            }
            if ( Vector3f.dot( clipVelocity, planes.get(k) ) >= 0.1 ) {
              continue; // move doesn't interact with the plane
            }

            // stop dead at a tripple plane interaction
            Vector3f.zero(m_Velocity);
            return true;
          }
        }

        // if we have fixed all interactions, try another move
        Vector3f.set( clipVelocity, m_Velocity );
        Vector3f.set( endClipVelocity, endVelocity );
        break;
      }
    }

    if ( gravity ) {
      Vector3f.set( endVelocity, m_Velocity );
    }

    return ( bumpcount != 0 );
  }

}