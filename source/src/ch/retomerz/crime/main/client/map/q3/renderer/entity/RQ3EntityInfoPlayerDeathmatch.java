/**
 * Created by merz on 14.05.14.
 */
package ch.retomerz.crime.main.client.map.q3.renderer.entity;

import java.util.Map;

public final class RQ3EntityInfoPlayerDeathmatch {

  /**
   * 0 = x
   * 1 = y
   * 2 = z
   * 3 = angle
   */
  public final int[] m_Dir;

  private RQ3EntityInfoPlayerDeathmatch(int[] _Dir) {
    m_Dir = _Dir;
  }

  public static RQ3EntityInfoPlayerDeathmatch of(Map<String, String> _Map) {
    final String origin = _Map.get("origin");
    if (null == origin) {
      throw new IllegalArgumentException("No origin: " + _Map);
    }
    final String angle = _Map.get("angle");
    if (null == angle) {
      throw new IllegalArgumentException("No angle: " + _Map);
    }
    final String[] origins = origin.split(" ");
    try {
      return new RQ3EntityInfoPlayerDeathmatch(new int[]{
              Integer.parseInt(origins[0]),
              Integer.parseInt(origins[1]),
              Integer.parseInt(origins[2]),
              Integer.parseInt(angle)
      });
    } catch (NumberFormatException e) {
      throw new IllegalArgumentException("Illegal origin or angle format: " + _Map);
    }
  }

}