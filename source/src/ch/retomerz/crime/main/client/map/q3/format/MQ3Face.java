/**
 * Created by merz on 01.05.14.
 */
package ch.retomerz.crime.main.client.map.q3.format;

import ch.retomerz.crime.main.server.temp.map.q3.LEDByteAccess;

import java.util.Arrays;

public final class MQ3Face {
    public int m_TextureIndex;                          // Texture index
    public int m_EffectIndex;                           // Index into lump 12 (Effects), or -1
    public int m_Type;                                  // Face type. 1 = Polygon, 2 = Patch, 3 = Mesh, 4 = Billboard
    public int m_Vertex;                                // Index of first vertex
    public int m_NbVertices;                            // Number of vertices
    public int m_MeshVertex;                            // Index of first meshvertex
    public int m_NbMeshVertices;                        // Number of meshvertices
    public int m_LightmapIndex;                         // Lightmap index
    public int[/* 2 */] m_LightmapCorner;               // Corner of this face's lightmap image in lightmap
    public int[/* 2 */] m_LightmapSize;                 // Size of this face's lightmap image in lightmap
    public float[/* 3 */] m_LightmapOrigin;             // World space origin of lightmap
    public float[/* 2 */][/* 3 */] m_LightmapVecs;      // World space lightmap s and t unit vectors
    public float[/* 3 */] m_Normal;                     // Surface normal
    public int[/* 2 */] m_PatchSize;                    // Patch dimensions

    @Override
    public String toString() {
        return "MQ3Face{" +
                "textureIndex=" + m_TextureIndex +
                ", effectIndex=" + m_EffectIndex +
                ", type=" + m_Type +
                ", vertex=" + m_Vertex +
                ", nbVertices=" + m_NbVertices +
                ", meshVertex=" + m_MeshVertex +
                ", nbMeshVertices=" + m_NbMeshVertices +
                ", lightmapIndex=" + m_LightmapIndex +
                ", lightmapCorner=" + Arrays.toString(m_LightmapCorner) +
                ", lightmapSize=" + Arrays.toString(m_LightmapSize) +
                ", lightmapOrigin=" + Arrays.toString(m_LightmapOrigin) +
                ", lightmapVecs=" + Arrays.toString(m_LightmapVecs) +
                ", normal=" + Arrays.toString(m_Normal) +
                ", patchSize=" + Arrays.toString(m_PatchSize) +
                '}';
    }

    public static final int SIZE_OF =
            LEDByteAccess.SIZE_OF_INT + // m_TextureIndex
            LEDByteAccess.SIZE_OF_INT + // m_EffectIndex
            LEDByteAccess.SIZE_OF_INT + // m_Type
            LEDByteAccess.SIZE_OF_INT + // m_Vertex
            LEDByteAccess.SIZE_OF_INT + // m_NbVertices
            LEDByteAccess.SIZE_OF_INT + // m_MeshVertex
            LEDByteAccess.SIZE_OF_INT + // m_NbMeshVertices
            LEDByteAccess.SIZE_OF_INT + // m_LightmapIndex
            (2*LEDByteAccess.SIZE_OF_INT) + // m_LightmapCorner
            (2*LEDByteAccess.SIZE_OF_INT) + // m_LightmapSize
            (3*LEDByteAccess.SIZE_OF_FLOAT) + // m_LightmapOrigin
            (2*3*LEDByteAccess.SIZE_OF_FLOAT) + // m_LightmapVecs
            (3*LEDByteAccess.SIZE_OF_FLOAT) + // m_Normal
            (2*LEDByteAccess.SIZE_OF_INT); // m_PatchSize

}