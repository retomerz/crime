/**
 * Created by merz on 01.05.14.
 */
package ch.retomerz.crime.main.client.map.q3.format;

import ch.retomerz.crime.main.server.temp.map.q3.LEDByteAccess;

public final class MQ3LeafFace {
    public int m_FaceIndex; // Face index.

    @Override
    public String toString() {
        return "MQ3LeafFace{" +
                "faceIndex=" + m_FaceIndex +
                '}';
    }

    public static final int SIZE_OF = LEDByteAccess.SIZE_OF_INT;
}