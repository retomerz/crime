/**
 * Created by merz on 04.05.14.
 */
package ch.retomerz.crime.main.client.map.q3.renderer;

import ch.retomerz.crime.main.client.map.q3.format.MQ3Face;

public final class RQ3Face {

    public final int m_Vertex;                                // Index of first vertex
    public final int m_NbMeshVertices;
    public final int m_MeshVertex;                            // Index of first meshvertex
    public int m_indexOffset;

    public RQ3Face(MQ3Face _Face) {
        m_Vertex = _Face.m_Vertex;
        m_NbMeshVertices = _Face.m_NbMeshVertices;
        m_MeshVertex = _Face.m_MeshVertex;
    }

}