/**
 * Created by merz on 21.08.2014.
 */
package ch.retomerz.crime.main.client.map.q3.renderer;

import ch.retomerz.crime.main.client.Resources;
import ch.retomerz.crime.main.client.input.BrowserMouse;
import ch.retomerz.crime.main.client.map.q3.format.MQ3LightMapElement;
import ch.retomerz.crime.main.client.math.Matrix4f;
import ch.retomerz.crime.main.client.util.JSArrayUtil;
import com.google.gwt.canvas.client.Canvas;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.DoubleClickEvent;
import com.google.gwt.event.dom.client.DoubleClickHandler;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.dom.client.MouseMoveEvent;
import com.google.gwt.event.dom.client.MouseMoveHandler;
import com.googlecode.gwtgl.array.Float32Array;
import com.googlecode.gwtgl.array.Uint16Array;
import com.googlecode.gwtgl.binding.WebGLBuffer;
import com.googlecode.gwtgl.binding.WebGLProgram;
import com.googlecode.gwtgl.binding.WebGLRenderingContext;
import com.googlecode.gwtgl.binding.WebGLShader;
import com.googlecode.gwtgl.binding.WebGLTexture;
import com.googlecode.gwtgl.binding.WebGLUniformLocation;

public final class RQ3Renderer {

  private static final int VERTEX_STRIDE = 56;

  // LATER: make this configurable
  private static final int PERSPECTIVE_FOV = 45;
  private static final float PERSPECTIVE_NEAR_DISTANCE = 0.1f;
  private static final float PERSPECTIVE_FAR_DISTANCE = 4096f;

  private final Canvas m_Canvas;
  private final WebGLRenderingContext gl;
  private WebGLProgram shaderProgram;
  private int m_AttributeVertexPosition;
  private int m_AttributeTextureCoord;
  private int m_AttributeTextureLightCoord;
  private int m_AttributeNormal;
  private int m_AttributeColor;
  private WebGLBuffer vertexBuffer;
  private WebGLBuffer m_IndexBuffer;
  private WebGLUniformLocation m_UniformModelViewMatrix;
  private WebGLUniformLocation m_UniformProjectionMatrix;
  private WebGLUniformLocation m_UniformTexture;
  private WebGLUniformLocation m_UniformLightmap;
  private WebGLTexture m_Lightmap;


  private BrowserMouse m_InputMouse;
  private final boolean[] m_InputKeyPressed;
  private final float[] m_InputDirection;
  private boolean m_MouseLocked;
  private RQ3Map m_Map;
  private Matrix4f m_ModelView;
  private Matrix4f m_Projection;
  private Matrix4f m_Camera;
  private RQ3Movement m_Movement;


  public RQ3Renderer(Canvas _Canvas, WebGLRenderingContext _gl, RQ3Map _Map) {
    m_Canvas = _Canvas;
    gl = _gl;
    m_Map = _Map;
    m_InputKeyPressed = new boolean[128];
    m_InputDirection = new float[3];
    m_InputMouse = new BrowserMouse();
    m_Lightmap = RQ3ShaderUtil.createSolidTexture(gl, new int[]{255,255,255,255});

    buildLightmaps();

    initParams();
    initShaders();
    initBuffers();
    for (RQ3Texture texture : _Map.m_Textures) {
      if (texture.isRenderable()) {
        initTextureFromImage(texture);
      }
    }
    initControl();

  }

  private void buildLightmaps() {
    gl.bindTexture(WebGLRenderingContext.TEXTURE_2D, m_Lightmap);
    gl.texImage2D(WebGLRenderingContext.TEXTURE_2D, 0, WebGLRenderingContext.RGBA, m_Map.m_LightMap.m_TextureSize, m_Map.m_LightMap.m_TextureSize, 0, WebGLRenderingContext.RGBA, WebGLRenderingContext.UNSIGNED_BYTE, null);
    gl.texParameteri(WebGLRenderingContext.TEXTURE_2D, WebGLRenderingContext.TEXTURE_MAG_FILTER, WebGLRenderingContext.LINEAR);
    gl.texParameteri(WebGLRenderingContext.TEXTURE_2D, WebGLRenderingContext.TEXTURE_MIN_FILTER, WebGLRenderingContext.LINEAR_MIPMAP_LINEAR);

    for (MQ3LightMapElement lightmap : m_Map.m_LightMap.m_Lightmaps) {
      gl.texSubImage2D(
              WebGLRenderingContext.TEXTURE_2D, 0, lightmap.m_X, lightmap.m_Y, lightmap.m_Width, lightmap.m_Height,
              WebGLRenderingContext.RGBA, WebGLRenderingContext.UNSIGNED_BYTE, JSArrayUtil.createArray(lightmap.m_Data)
      );
    }

    gl.generateMipmap(WebGLRenderingContext.TEXTURE_2D);

    //q3glshader.init(gl, this.lightmap); TODO ?
  }

  private void initParams() {
    int width = 500;
    int height = 500;
    //gl.viewport(0, 0, canvas.getOffsetWidth(), canvas.getOffsetHeight());
    gl.viewport(0, 0, width, height);
    gl.clearColor(0.2f, 0.0f, 0.0f, 1.0f);
    gl.clearDepth(1.0f);
    gl.enable(WebGLRenderingContext.DEPTH_TEST);
    gl.enable(WebGLRenderingContext.BLEND);
    gl.depthFunc(WebGLRenderingContext.LEQUAL);


    m_ModelView = new Matrix4f();
    m_Projection = new Matrix4f();
    m_Camera = new Matrix4f();
    m_Movement = new RQ3Movement(m_Map);
    m_Movement.spawn(m_Map.m_Entities);
    m_Projection.perspective(PERSPECTIVE_FOV, width/height, PERSPECTIVE_NEAR_DISTANCE, PERSPECTIVE_FAR_DISTANCE);

    checkErrors();
  }

  private void initShaders() {
    WebGLShader fragmentShader = getShader(WebGLRenderingContext.FRAGMENT_SHADER, Resources.INSTANCE.textureFragmentShader().getText());
    WebGLShader vertexShader = getShader(WebGLRenderingContext.VERTEX_SHADER, Resources.INSTANCE.textureVertexShader().getText());

    // create the ShaderProgram and attach the Shaders
    shaderProgram = gl.createProgram();
    if (shaderProgram == null || gl.getError() != WebGLRenderingContext.NO_ERROR) {
      throw new RuntimeException("program error");
    }

    gl.attachShader(shaderProgram, vertexShader);
    gl.attachShader(shaderProgram, fragmentShader);

    // Bind vertexPosition to attribute 0
    gl.bindAttribLocation(shaderProgram, 0, "vertexPosition");
    // Bind texPosition to attribute 1
    gl.bindAttribLocation(shaderProgram, 1, "texPosition");
    gl.bindAttribLocation(shaderProgram, 2, "lightCoord");
    gl.bindAttribLocation(shaderProgram, 3, "color");

    // Link the Shader Program
    gl.linkProgram(shaderProgram);
    if (!gl.getProgramParameterb(shaderProgram, WebGLRenderingContext.LINK_STATUS)) {
      throw new RuntimeException("Could not initialise shaders: " + gl.getProgramInfoLog(shaderProgram));
    }

    // Set the ShaderProgram active
    gl.useProgram(shaderProgram);

    m_AttributeVertexPosition = gl.getAttribLocation(shaderProgram, "vertexPosition");
    gl.enableVertexAttribArray(m_AttributeVertexPosition);

    //m_AttributeNormal = gl.getAttribLocation(shaderProgram, "normal");
    //gl.enableVertexAttribArray(m_AttributeNormal);

    m_AttributeTextureCoord = gl.getAttribLocation(shaderProgram, "texPosition");
    gl.enableVertexAttribArray(m_AttributeTextureCoord);

    m_AttributeTextureLightCoord = gl.getAttribLocation(shaderProgram, "lightCoord");
    gl.enableVertexAttribArray(m_AttributeTextureLightCoord);

    m_AttributeColor = gl.getAttribLocation(shaderProgram, "color");
    gl.enableVertexAttribArray(m_AttributeColor);

    // get the position of the projectionMatrix uniform.
    m_UniformModelViewMatrix = gl.getUniformLocation(shaderProgram, "modelViewMatrix");
    m_UniformProjectionMatrix = gl.getUniformLocation(shaderProgram, "projectionMatrix");

    m_UniformTexture = gl.getUniformLocation(shaderProgram, "tex");
    m_UniformLightmap = gl.getUniformLocation(shaderProgram, "lightmap");

    checkErrors();
  }

  private WebGLShader getShader(int type, String source) {
    final WebGLShader shader = gl.createShader(type);
    gl.shaderSource(shader, source);
    gl.compileShader(shader);
    checkErrors();

    if (!gl.getShaderParameterb(shader, WebGLRenderingContext.COMPILE_STATUS)) {
      throw new RuntimeException(gl.getShaderInfoLog(shader));
    }
    return shader;
  }

  private void initBuffers() {
    vertexBuffer = gl.createBuffer();
    gl.bindBuffer(WebGLRenderingContext.ARRAY_BUFFER, vertexBuffer);
    gl.bufferData(WebGLRenderingContext.ARRAY_BUFFER,
            Float32Array.create(/*TestCube.m_Vertex*/m_Map.m_Vertices),
            WebGLRenderingContext.STATIC_DRAW);

    m_IndexBuffer = gl.createBuffer();
    gl.bindBuffer(WebGLRenderingContext.ELEMENT_ARRAY_BUFFER, m_IndexBuffer);
    gl.bufferData(WebGLRenderingContext.ELEMENT_ARRAY_BUFFER, Uint16Array.create(m_Map.m_Indices), WebGLRenderingContext.STATIC_DRAW);

    checkErrors();
  }

  private void initTextureFromImage(RQ3Texture _Texture) {
    _Texture.m_Texture = gl.createTexture();
    gl.bindTexture(WebGLRenderingContext.TEXTURE_2D, _Texture.m_Texture);
    gl.texParameteri(WebGLRenderingContext.TEXTURE_2D, WebGLRenderingContext.TEXTURE_MAG_FILTER, WebGLRenderingContext.LINEAR);
    gl.texParameteri(WebGLRenderingContext.TEXTURE_2D, WebGLRenderingContext.TEXTURE_MIN_FILTER, WebGLRenderingContext.LINEAR_MIPMAP_NEAREST);
    gl.texImage2D(WebGLRenderingContext.TEXTURE_2D, 0, WebGLRenderingContext.RGBA, WebGLRenderingContext.RGBA, WebGLRenderingContext.UNSIGNED_BYTE, _Texture.m_Image.getElement());
    gl.generateMipmap(WebGLRenderingContext.TEXTURE_2D);
    checkErrors();
  }

  private void initControl() {
    m_Canvas.addKeyDownHandler(new KeyDownHandler() {
      @Override
      public void onKeyDown(KeyDownEvent event) {
        final int keyCode = event.getNativeKeyCode();
        if (32 == keyCode && !m_InputKeyPressed[32]) {
          m_Movement.jump();
        }
        m_InputKeyPressed[keyCode] = true;
      }
    });

    m_Canvas.addKeyUpHandler(new KeyUpHandler() {
      @Override
      public void onKeyUp(KeyUpEvent event) {
        m_InputKeyPressed[event.getNativeKeyCode()] = false;
      }
    });

    m_Canvas.addClickHandler(new ClickHandler() {
      @Override public void onClick(ClickEvent event) {
        if (NativeEvent.BUTTON_RIGHT == event.getNativeButton()) {
          m_Movement.jump();
        }
      }
    });

    m_Canvas.addDoubleClickHandler(new DoubleClickHandler() {
      @Override public void onDoubleClick(DoubleClickEvent event) {
        m_MouseLocked = !m_InputMouse.isCursorCatched();
        m_InputMouse.setCursorCatched(m_Canvas.getCanvasElement(), m_MouseLocked);
      }
    });

    m_Canvas.addMouseMoveHandler(new MouseMoveHandler() {
      @Override
      public void onMouseMove(MouseMoveEvent event) {
        if (m_MouseLocked) {
          m_Movement.look(m_InputMouse.getMovementXJSNI(event.getNativeEvent()), m_InputMouse.getMovementYJSNI(event.getNativeEvent()));
        }
      }
    });

  }

  public void draw(final double _FrameTime) {
    updateInput(_FrameTime);

    m_ModelView.identity();
    m_ModelView.rotateX((float) (m_Movement.m_AngleX - Math.PI / 2));
    m_ModelView.rotateZ(m_Movement.m_AngleZ);
    m_ModelView.translate(-m_Movement.m_Position[0], -m_Movement.m_Position[1], -m_Movement.m_Position[2]-30); // TODO -30 ?

    gl.clear(WebGLRenderingContext.COLOR_BUFFER_BIT | WebGLRenderingContext.DEPTH_BUFFER_BIT);

    // Load the vertex data
    gl.bindBuffer(WebGLRenderingContext.ELEMENT_ARRAY_BUFFER, m_IndexBuffer);
    gl.bindBuffer(WebGLRenderingContext.ARRAY_BUFFER, vertexBuffer);
    gl.vertexAttribPointer(m_AttributeVertexPosition, 3, WebGLRenderingContext.FLOAT, false, VERTEX_STRIDE, 0);
    gl.vertexAttribPointer(m_AttributeTextureCoord, 2, WebGLRenderingContext.FLOAT, false, VERTEX_STRIDE, 3*4);
    gl.vertexAttribPointer(m_AttributeTextureLightCoord, 2, WebGLRenderingContext.FLOAT, false, VERTEX_STRIDE, 5*4);
    //gl.vertexAttribPointer(m_AttributeNormal, 3, WebGLRenderingContext.FLOAT, false, VERTEX_STRIDE, 7*4);
    gl.vertexAttribPointer(m_AttributeColor, 4, WebGLRenderingContext.FLOAT, false, VERTEX_STRIDE, 10*4);

    gl.uniformMatrix4fv(m_UniformModelViewMatrix, false, m_ModelView.a);
    gl.uniformMatrix4fv(m_UniformProjectionMatrix, false, m_Projection.a);

    gl.blendFunc(WebGLRenderingContext.ONE, WebGLRenderingContext.ZERO);
    gl.depthMask(true);
    gl.depthFunc(WebGLRenderingContext.LEQUAL);

    gl.activeTexture(WebGLRenderingContext.TEXTURE1);
    gl.uniform1i(m_UniformLightmap, 1);
    gl.bindTexture(WebGLRenderingContext.TEXTURE_2D, m_Lightmap);

    for (RQ3Texture texture : m_Map.m_Textures) {
      if (texture.isRenderable()) {
        gl.activeTexture(WebGLRenderingContext.TEXTURE0);
        gl.uniform1i(m_UniformTexture, 0);
        gl.bindTexture(WebGLRenderingContext.TEXTURE_2D, texture.m_Texture);
        gl.drawElements(WebGLRenderingContext.TRIANGLES, texture.m_elementCount, WebGLRenderingContext.UNSIGNED_SHORT, texture.m_indexOffset);
      }
    }
    gl.flush();
    checkErrors();
  }

  private void updateInput(final double _FrameTime) {
    final float[] d = m_InputDirection;
    d[0] = 0;
    d[1] = 0;
    d[2] = 0;

    final boolean[] k = m_InputKeyPressed;
    if (k['W']) {
      d[1] += 1;
    }
    if (k['S']) {
      d[1] -= 1;
    }
    if (k['A']) {
      d[0] -= 1;
    }
    if (k['D']) {
      d[0] += 1;
    }

    if (0 != d[0] || 0 != d[1] || 0 != d[2]) {
      final Matrix4f camera = m_Camera;
      camera.identity();
      camera.rotateZ(m_Movement.m_AngleZ);
      camera.inverse();
      Matrix4f.multiply3f(camera, d);
    }

    m_Movement.move(d, _FrameTime);
  }

  private void checkErrors() {
    StringBuilder errors = null;
    int error;
    while ((error = gl.getError()) != WebGLRenderingContext.NO_ERROR) {
      final String message;
      switch (error) {
        case 0x0500:
          message = "GL_INVALID_ENUM";
          break;
        case 0x0501:
          message = "GL_INVALID_VALUE";
          break;
        case 0x0502:
          message = "GL_INVALID_OPERATION";
          break;
        case 0x0503:
          message = "GL_STACK_OVERFLOW";
          break;
        case 0x0504:
          message = "GL_STACK_UNDERFLOW";
          break;
        case 0x0505:
          message = "GL_OUT_OF_MEMORY";
          break;
        case 0x0506:
          message = "GL_INVALID_FRAMEBUFFER_OPERATION";
          break;
        case 0x8031:
          message = "GL_TABLE_TOO_LARGE";
          break;
        default:
          message = "Unknown WebGL Error: " + error;
          break;
      }
      if (null == errors) {
        errors = new StringBuilder(message);
      } else {
        errors.append("\n").append(message);
      }
    }
    if (null != errors) {
      GWT.log(errors.toString(), null);
      throw new RuntimeException(errors.toString());
    }
  }

}