/**
 * Created by merz on 01.05.14.
 */
package ch.retomerz.crime.main.client.map.q3.format;

import ch.retomerz.crime.main.server.temp.map.q3.LEDByteAccess;

import java.util.Arrays;

public final class MQ3Texture {
    public char[/* 64 */] m_Name;   // Texture name.
    public int m_Flags;             // Surface flags.
    public int m_Contents;          // Content flags.

    @Override
    public String toString() {
        return "MQ3Texture{" +
                "name=" + new String(m_Name) +
                ", flags=" + m_Flags +
                ", contents=" + m_Contents +
                '}';
    }

    public static final int SIZE_OF =
            (64*LEDByteAccess.SIZE_OF_CHAR) +
            LEDByteAccess.SIZE_OF_INT +
            LEDByteAccess.SIZE_OF_INT;
}