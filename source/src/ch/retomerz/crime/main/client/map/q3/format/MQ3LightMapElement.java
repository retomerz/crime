/**
 * Created by merz on 20.05.14.
 */
package ch.retomerz.crime.main.client.map.q3.format;

public final class MQ3LightMapElement {

  public final int m_X;
  public final int m_Y;
  public final int m_Width;
  public final int m_Height;
  public final int[] m_Data;

  public MQ3LightMapElement(int _X, int _Y, int _Width, int _Height, int[] _Data) {
    m_X = _X;
    m_Y = _Y;
    m_Width = _Width;
    m_Height = _Height;
    m_Data = _Data;
  }

}