/**
 * Created by merz on 20.08.2014.
 */
package ch.retomerz.crime.main.client.map.q3.renderer;

import ch.retomerz.crime.main.client.map.q3.format.MQ3Brush;
import ch.retomerz.crime.main.client.map.q3.format.MQ3BrushSide;
import ch.retomerz.crime.main.client.map.q3.format.MQ3Face;
import ch.retomerz.crime.main.client.map.q3.format.MQ3Leaf;
import ch.retomerz.crime.main.client.map.q3.format.MQ3LeafBrush;
import ch.retomerz.crime.main.client.map.q3.format.MQ3LightMap;
import ch.retomerz.crime.main.client.map.q3.format.MQ3LightMapElementRect;
import ch.retomerz.crime.main.client.map.q3.format.MQ3Map;
import ch.retomerz.crime.main.client.map.q3.format.MQ3Node;
import ch.retomerz.crime.main.client.map.q3.format.MQ3Plane;
import ch.retomerz.crime.main.client.map.q3.format.MQ3Texture;
import ch.retomerz.crime.main.client.map.q3.format.MQ3Vertex;
import ch.retomerz.crime.main.client.math.Vector3f;
import ch.retomerz.crime.main.client.util.CollectionUtil;

import java.util.ArrayList;
import java.util.List;

public final class RQ3Map {

  private static final float q3bsptree_trace_offset = 0.03125f;

  public final RQ3Entities m_Entities;
  private final List<MQ3Plane> m_Planes;
  private final List<MQ3Node> m_Nodes;
  private final List<MQ3Leaf> m_Leaves;
  private final List<MQ3Brush> m_Brushes;
  private final List<MQ3LeafBrush> m_LeafBrushes;
  private final List<MQ3BrushSide> m_BrushSides;
  public final MQ3LightMap m_LightMap;
  public final List<RQ3Texture> m_Textures;
  public final int[] m_Indices;
  public final float[] m_Vertices;

  public RQ3Map(MQ3Map _Map) {

    m_Textures = new ArrayList<RQ3Texture>();
    m_Entities = RQ3Entities.of(_Map.m_Entity);
    m_Planes = _Map.m_Planes;
    m_Nodes = _Map.m_Nodes;
    m_Leaves = _Map.m_Leaves;
    m_Brushes = _Map.m_Brushes;
    m_LeafBrushes = _Map.m_LeafBrushes;
    m_BrushSides = _Map.m_BrushSides;
    m_LightMap = _Map.m_LightMap;

    RQ3Texture texture;
    for (MQ3Texture t : _Map.m_Textures) {
      texture = new RQ3Texture(t);
      buildDefault(texture);
      m_Textures.add(texture);
    }

    for (int i = 0; i <  _Map.m_Faces.size(); ++i) {
      MQ3Face face = _Map.m_Faces.get(i);
      if (1 == face.m_Type || 2 == face.m_Type || 3 == face.m_Type) {
        texture = m_Textures.get(face.m_TextureIndex);
        texture.m_Faces.add(new RQ3Face(face));
        final MQ3LightMapElementRect lightmap = _Map.m_LightMap.m_LightmapRects.get(face.m_LightmapIndex);

        if (1 == face.m_Type || 3 == face.m_Type) {
          texture.geomType = face.m_Type;

          MQ3Vertex vertex;
          for (int j = 0; j < face.m_NbMeshVertices; ++j) {
            vertex = _Map.m_Vertices.get(face.m_Vertex + _Map.m_MeshVertices.get(face.m_MeshVertex+j).m_MeshVert);
            System.out.println("LM i=" + i + ", j=" + j +
                            ", lm0=" + vertex.m_TexCoordLight[0] +
                            ", lm1=" + vertex.m_TexCoordLight[1] +
                            ", xs=" + lightmap.m_XScale +
                            ", ys=" + lightmap.m_YScale +
                            ", x=" + lightmap.m_X +
                            ", y=" + lightmap.m_Y
            );
            vertex.lmNewCoord[0] = (float)((vertex.m_TexCoordLight[0] * lightmap.m_XScale) + lightmap.m_X);
            vertex.lmNewCoord[1] = (float)((vertex.m_TexCoordLight[1] * lightmap.m_YScale) + lightmap.m_Y);
          }

        } else {
          //throw new RuntimeException("TODO"); // TODO MERZ
        }
      } else {
        throw new RuntimeException("TODO");
      }
    }

    m_Vertices = new float[_Map.m_Vertices.size()*14];
    int off  =0;
    for (MQ3Vertex vertex : _Map.m_Vertices) {

      m_Vertices[off++] = vertex.m_Position[0];
      m_Vertices[off++] = vertex.m_Position[1];
      m_Vertices[off++] = vertex.m_Position[2];

      m_Vertices[off++] = vertex.m_TexCoord[0];
      m_Vertices[off++] = vertex.m_TexCoord[1];

      m_Vertices[off++] = vertex.lmNewCoord[0];
      m_Vertices[off++] = vertex.lmNewCoord[1];

      m_Vertices[off++] = vertex.m_Normal[0];
      m_Vertices[off++] = vertex.m_Normal[1];
      m_Vertices[off++] = vertex.m_Normal[2];

      m_Vertices[off++] = vertex.m_Color[0];
      m_Vertices[off++] = vertex.m_Color[1];
      m_Vertices[off++] = vertex.m_Color[2];
      m_Vertices[off++] = vertex.m_Color[3];

    }

    final List<Integer> indices = new ArrayList<Integer>();
    for (RQ3Texture t : m_Textures) {
      t.m_indexOffset = indices.size()*2;
      for (RQ3Face face : t.m_Faces) {
        face.m_indexOffset = indices.size()*2;
        for (int i = 0; i < face.m_NbMeshVertices; i++) {
          indices.add(face.m_Vertex + _Map.m_MeshVertices.get(face.m_MeshVertex+i).m_MeshVert);
        }
        t.m_elementCount += face.m_NbMeshVertices;
      }
    }
    m_Indices = CollectionUtil.integerToArray(indices);

  }

  private RQ3Shader buildDefault(RQ3Texture _Texture) {
    final RQ3ShaderStage diffuseStage = new RQ3ShaderStage();
    diffuseStage.m_Map = _Texture.m_Name;
    return new RQ3Shader(diffuseStage);
  }

  public void trace(RQ3TraceResult _Result, float[] start, float[] end, float radius) {
    _Result.init(end);

    traceNode(0, 0, 1, start, end, radius, _Result);

    if (_Result.m_Fraction != 1.0) { // collided with something
      for (int i = 0; i < 3; i++) {
        _Result.m_EndPos[i] = start[i] + _Result.m_Fraction * (end[i] - start[i]);
      }
    }

  }

  private void traceNode(int nodeIdx, float startFraction, float endFraction, float[] start, float[] end, float radius, RQ3TraceResult output) {
    if (nodeIdx < 0) { // Leaf node?
      MQ3Leaf leaf = m_Leaves.get(-(nodeIdx + 1));
      for (int i = 0; i < leaf.m_NbLeafBrushes; i++) {
        MQ3Brush brush = m_Brushes.get(m_LeafBrushes.get(leaf.m_LeafBrush + i).m_BrushIndex);
        RQ3Texture surface = m_Textures.get(brush.m_TextureIndex);
        // TODO MERZ CHECK: original code: if (brush.m_NbBrushSides > 0 && surface.m_Contents & 1) {
        if (brush.m_NbBrushSides > 0 && 0 != (surface.m_Contents & 1)) {
          traceBrush(brush, start, end, radius, output);
        }
      }
      return;
    }

    // Tree node
    MQ3Node node = m_Nodes.get(nodeIdx);
    MQ3Plane plane = m_Planes.get(node.m_Plane);

    float startDist = Vector3f.dot(plane.m_Normal, start) - plane.m_Distance;
    float endDist = Vector3f.dot(plane.m_Normal, end) - plane.m_Distance;

    if (startDist >= radius && endDist >= radius) {
      this.traceNode(node.m_Children[0], startFraction, endFraction, start, end, radius, output );
    } else if (startDist < -radius && endDist < -radius) {
      this.traceNode(node.m_Children[1], startFraction, endFraction, start, end, radius, output );
    } else {
      int side;
      float fraction1, fraction2, middleFraction;
      float[] middle = new float[3];

      if (startDist < endDist) {
        side = 1; // back
        float iDist = 1 / (startDist - endDist);
        fraction1 = (startDist - radius + q3bsptree_trace_offset) * iDist;
        fraction2 = (startDist + radius + q3bsptree_trace_offset) * iDist;
      } else if (startDist > endDist) {
        side = 0; // front
        float iDist = 1 / (startDist - endDist);
        fraction1 = (startDist + radius + q3bsptree_trace_offset) * iDist;
        fraction2 = (startDist - radius - q3bsptree_trace_offset) * iDist;
      } else {
        side = 0; // front
        fraction1 = 1;
        fraction2 = 0;
      }

      if (fraction1 < 0) fraction1 = 0;
      else if (fraction1 > 1) fraction1 = 1;
      if (fraction2 < 0) fraction2 = 0;
      else if (fraction2 > 1) fraction2 = 1;

      middleFraction = startFraction + (endFraction - startFraction) * fraction1;

      for (int i = 0; i < 3; i++) {
        middle[i] = start[i] + fraction1 * (end[i] - start[i]);
      }

      traceNode(node.m_Children[side], startFraction, middleFraction, start, middle, radius, output);

      middleFraction = startFraction + (endFraction - startFraction) * fraction2;

      for (int i = 0; i < 3; i++) {
        middle[i] = start[i] + fraction2 * (end[i] - start[i]);
      }

      traceNode(node.m_Children[side == 0 ? 1 : 0], middleFraction, endFraction, middle, end, radius, output);
    }
  }

  private void traceBrush(MQ3Brush brush, float[] start, float[] end, float radius, RQ3TraceResult output) {
    double startFraction = -1;
    double endFraction = 1;
    boolean startsOut = false;
    boolean endsOut = false;
    MQ3Plane collisionPlane = null;

    for (int i = 0; i < brush.m_NbBrushSides; i++) {
      MQ3BrushSide brushSide = m_BrushSides.get(brush.m_BrushSide + i);
      MQ3Plane plane = m_Planes.get(brushSide.m_PlaneIndex);

      float startDist = Vector3f.dot(start, plane.m_Normal) - (plane.m_Distance + radius);
      float endDist = Vector3f.dot(end, plane.m_Normal ) - (plane.m_Distance + radius);

      if (startDist > 0) startsOut = true;
      if (endDist > 0) endsOut = true;

      // make sure the trace isn't completely on one side of the brush
      if (startDist > 0 && endDist > 0) { return; }
      if (startDist <= 0 && endDist <= 0) { continue; }

      if (startDist > endDist) { // line is entering into the brush
        double fraction = (startDist - 0.03125) / (startDist - endDist);
        if (fraction > startFraction) {
          startFraction = fraction;
          collisionPlane = plane;
        }
      } else { // line is leaving the brush
        double fraction = (startDist + 0.03125) / (startDist - endDist);
        if (fraction < endFraction)
          endFraction = fraction;
      }
    }

    if (!startsOut) {
      output.m_StartsOut = false;
      if (!endsOut)
        output.m_AllSolid = true;
      return;
    }

    if (startFraction < endFraction) {
      if (startFraction > -1 && startFraction < output.m_Fraction) {
        output.m_Plane = collisionPlane;
        if (startFraction < 0)
          startFraction = 0;
        output.m_Fraction = (float)startFraction;
      }
    }

  }

}