/**
 * Created by merz on 21.05.14.
 */
package ch.retomerz.crime.main.client.map.q3.renderer;

import com.googlecode.gwtgl.array.Uint8Array;
import com.googlecode.gwtgl.binding.WebGLRenderingContext;
import com.googlecode.gwtgl.binding.WebGLTexture;

public final class RQ3ShaderUtil {

  private RQ3ShaderUtil() {
  }

  public static WebGLTexture createSolidTexture(final WebGLRenderingContext gl, final int[] _Color) {
    final Uint8Array data = Uint8Array.create(_Color);
    final WebGLTexture texture = gl.createTexture();
    gl.bindTexture(WebGLRenderingContext.TEXTURE_2D, texture);
    gl.texImage2D(WebGLRenderingContext.TEXTURE_2D, 0, WebGLRenderingContext.RGB, 1, 1, 0, WebGLRenderingContext.RGB, WebGLRenderingContext.UNSIGNED_BYTE, data);
    gl.texParameteri(WebGLRenderingContext.TEXTURE_2D, WebGLRenderingContext.TEXTURE_MAG_FILTER, WebGLRenderingContext.NEAREST);
    gl.texParameteri(WebGLRenderingContext.TEXTURE_2D, WebGLRenderingContext.TEXTURE_MIN_FILTER, WebGLRenderingContext.NEAREST);
    return texture;
  }

}