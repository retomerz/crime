/**
 * Created by merz on 04.05.14.
 */
package ch.retomerz.crime.main.client.map.q3;

public final class LEDByteAccess {

    public static final int SIZE_OF_CHAR = 1;
    public static final int SIZE_OF_INT = 4;
    public static final int SIZE_OF_FLOAT = 4;

    private final byte[] m_Data;
    private final byte[] m_Buffer = new byte[8];
    private int m_Pos;

    public LEDByteAccess(byte [] _Data) {
        m_Data = _Data;
        m_Pos = 0;
    }

    public void seek(int _Pos) {
        m_Pos = _Pos;
    }

  // TODO U ?
  public byte readUByte() {
    return m_Data[m_Pos++];
  }

    private byte readByte() {
        return m_Data[m_Pos++];
    }

    public char[] readChar(int _Count) {
        final char[] ret = new char[_Count];
        for (int i = 0; i < _Count; i++) {
            ret[i] = (char)readByte();
        }
        return ret;
    }

    // TODO unsigned ?
    public char[] readCharUnsigned(int _Count) {
        final char[] ret = new char[_Count];
        for (int i = 0; i < _Count; i++) {
            ret[i] = (char)readByte();
        }
        return ret;
    }

    // TODO unsigned ?
    // TODO a b c order correct ?
    public char[][][] readChar3Unsigned(int _CountA, int _CountB, int _CountC) {
        final char[][][] ret = new char[_CountA][_CountB][_CountC];
        for (int a = 0; a < _CountA; a++) {
            for (int b = 0; b < _CountB; b++) {
                for (int c = 0; c < _CountC; c++) {
                    ret[a][b][c] = (char)readByte();
                }
            }
        }
        return ret;
    }

    public float readFloat() {
        return Float.intBitsToFloat(readInt());
    }

    public float[] readFloat(int _Count) {
        final float[] ret = new float[_Count];
        for (int i = 0; i < _Count; i++) {
            ret[i] = readFloat();
        }
        return ret;
    }

    // TODO
    public float[][] readFloat2(int _CountA, int _CountB) {
        final float[][] ret = new float[_CountA][_CountB];
        for (int a = 0; a < _CountA; a++) {
            for (int b = 0; b < _CountB; b++) {
                ret[a][b] = readFloat();
            }
        }
        return ret;
    }

    public int readInt() {
        m_Buffer[0] = readByte();
        m_Buffer[1] = readByte();
        m_Buffer[2] = readByte();
        m_Buffer[3] = readByte();
        return (m_Buffer[3]) << 24 | (m_Buffer[2] & 0xff) << 16 | (m_Buffer[1] & 0xff) << 8 | (m_Buffer[0] & 0xff);
    }

    public int[] readInt(int _Count) {
        final int[] ret = new int[_Count];
        for (int i = 0; i < _Count; i++) {
            ret[i] = readInt();
        }
        return ret;
    }

  public final long readLittleLong() {
    m_Buffer[0] = readByte();
    m_Buffer[1] = readByte();
    m_Buffer[2] = readByte();
    m_Buffer[3] = readByte();
    m_Buffer[4] = readByte();
    m_Buffer[5] = readByte();
    m_Buffer[6] = readByte();
    m_Buffer[7] = readByte();
    return (long)(m_Buffer[7]) << 56 | (long)(m_Buffer[6]&0xff) << 48 |
            (long)(m_Buffer[5] & 0xff) << 40 | (long)(m_Buffer[4] & 0xff) << 32 |
            (long)(m_Buffer[3] & 0xff) << 24 | (long)(m_Buffer[2] & 0xff) << 16 |
            (long)(m_Buffer[1] & 0xff) <<  8 | (long)(m_Buffer[0] & 0xff);
  }

}