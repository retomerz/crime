/**
 * Created by merz on 01.05.14.
 */
package ch.retomerz.crime.main.client.map.q3.format;

import ch.retomerz.crime.main.server.temp.map.q3.LEDByteAccess;

import java.util.Arrays;

public final class MQ3Leaf {
    public int m_Cluster;           // Visdata cluster index.
    public int m_Area;              // Areaportal area.
    public int[/* 3 */] m_Mins;     // Integer bounding box min coord.
    public int[/* 3 */] m_Maxs;     // Integer bounding box max coord.
    public int m_LeafFace;          // First leafface for leaf.
    public int m_NbLeafFaces;       // Number of leaffaces for leaf.
    public int m_LeafBrush;         // First leafbrush for leaf.
    public int m_NbLeafBrushes;     // Number of leafbrushes for leaf.

    @Override
    public String toString() {
        return "MQ3Leaf{" +
                "cluster=" + m_Cluster +
                ", area=" + m_Area +
                ", mins=" + Arrays.toString(m_Mins) +
                ", maxs=" + Arrays.toString(m_Maxs) +
                ", leafFace=" + m_LeafFace +
                ", nbLeafFaces=" + m_NbLeafFaces +
                ", leafBrush=" + m_LeafBrush +
                ", nbLeafBrushes=" + m_NbLeafBrushes +
                '}';
    }

    public static final int SIZE_OF =
            LEDByteAccess.SIZE_OF_INT +
            LEDByteAccess.SIZE_OF_INT +
            (3*LEDByteAccess.SIZE_OF_INT) +
            (3*LEDByteAccess.SIZE_OF_INT) +
            LEDByteAccess.SIZE_OF_INT +
            LEDByteAccess.SIZE_OF_INT +
            LEDByteAccess.SIZE_OF_INT +
            LEDByteAccess.SIZE_OF_INT;

}