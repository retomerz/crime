/**
 * Created by merz on 20.05.14.
 */
package ch.retomerz.crime.main.client.map.q3.format;

public final class MQ3LightMapElementRect {

  public final double m_X;
  public final double m_Y;
  public final double m_XScale;
  public final double m_YScale;

  public MQ3LightMapElementRect(double _X, double _Y, double _XScale, double _YScale) {
    m_X = _X;
    m_Y = _Y;
    m_XScale = _XScale;
    m_YScale = _YScale;
  }

}