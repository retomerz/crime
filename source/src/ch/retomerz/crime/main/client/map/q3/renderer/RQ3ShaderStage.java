/**
 * Created by merz on 24.05.14.
 */
package ch.retomerz.crime.main.client.map.q3.renderer;

import com.googlecode.gwtgl.binding.WebGLRenderingContext;

public final class RQ3ShaderStage {

  public String m_Map;
  public boolean m_Lightmap;
  //public int m_BlendSrc = WebGLRenderingContext.ONE;
  public int m_BlendSrc = WebGLRenderingContext.DST_COLOR;
  public int m_BlendDest = WebGLRenderingContext.ZERO;
  public int m_DepthFunc = WebGLRenderingContext.LEQUAL;
  public boolean m_DepthWrite = true;

}