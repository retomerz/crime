/**
 * Created by merz on 01.05.14.
 */
package ch.retomerz.crime.main.client.map.q3.format;

import ch.retomerz.crime.main.server.temp.map.q3.LEDByteAccess;

import java.util.Arrays;
import java.util.List;

public final class MQ3LightMap {

  public final int m_TextureSize;
  public final List<MQ3LightMapElement> m_Lightmaps;
  public final List<MQ3LightMapElementRect> m_LightmapRects;

  public MQ3LightMap(int _TextureSize, List<MQ3LightMapElement> _Lightmaps, List<MQ3LightMapElementRect> _LightmapRects) {
    m_TextureSize = _TextureSize;
    m_Lightmaps = _Lightmaps;
    m_LightmapRects = _LightmapRects;
  }

  //public /*unsigned*/ char[/* 128 */][/* 128 */][/* 3 */] m_MapData; // Lightmap color data. RGB

    /*@Override
    public String toString() {
        return "MQ3LightMap{" +
                "mapData=" + Arrays.toString(m_MapData) +
                '}';
    }

    public static final int SIZE_OF =
            (128*128*3* LEDByteAccess.SIZE_OF_CHAR);*/
}