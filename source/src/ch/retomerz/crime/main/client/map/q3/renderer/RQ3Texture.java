/**
 * Created by merz on 04.05.14.
 */
package ch.retomerz.crime.main.client.map.q3.renderer;

import ch.retomerz.crime.main.client.map.q3.format.MQ3Texture;
import com.google.gwt.user.client.ui.Image;
import com.googlecode.gwtgl.binding.WebGLTexture;

import java.util.ArrayList;
import java.util.List;

public final class RQ3Texture {

  public final String m_Name;
  public final List<RQ3Face> m_Faces;
  public int m_indexOffset;
  public int m_elementCount;
  public final int m_Contents;
  public int geomType;
  public Image m_Image;
  public WebGLTexture m_Texture;

  @SuppressWarnings("Convert2Diamond") // JS compiler
  public RQ3Texture(MQ3Texture _Texture) {
    m_Name = parseName(_Texture.m_Name);
    m_Faces = new ArrayList<RQ3Face>();
    m_Contents = _Texture.m_Contents;
  }

  private static String parseName(char[] _Name) {
    String ret = new String(_Name);
    final int pos = ret.indexOf("\u0000");
    if (-1 != pos) {
      ret = ret.substring(0, pos);
    }
    return ret;
  }

  public boolean isRenderable() {
    // q3bsp.js -> if(surface.elementCount === 0 || surface.shader || surface.shaderName == 'noshader') {
    return m_elementCount > 0;
  }

}