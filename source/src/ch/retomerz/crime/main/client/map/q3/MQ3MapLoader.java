/**
 * Created by merz on 01.05.14.
 */
package ch.retomerz.crime.main.client.map.q3;

import ch.retomerz.crime.main.client.map.q3.format.MQ3Brush;
import ch.retomerz.crime.main.client.map.q3.format.MQ3BrushSide;
import ch.retomerz.crime.main.client.map.q3.format.MQ3Effect;
import ch.retomerz.crime.main.client.map.q3.format.MQ3Entity;
import ch.retomerz.crime.main.client.map.q3.format.MQ3Face;
import ch.retomerz.crime.main.client.map.q3.format.MQ3Header;
import ch.retomerz.crime.main.client.map.q3.format.MQ3Leaf;
import ch.retomerz.crime.main.client.map.q3.format.MQ3LeafBrush;
import ch.retomerz.crime.main.client.map.q3.format.MQ3LeafFace;
import ch.retomerz.crime.main.client.map.q3.format.MQ3LightMap;
import ch.retomerz.crime.main.client.map.q3.format.MQ3LightMapElement;
import ch.retomerz.crime.main.client.map.q3.format.MQ3LightMapElementRect;
import ch.retomerz.crime.main.client.map.q3.format.MQ3LightVol;
import ch.retomerz.crime.main.client.map.q3.format.MQ3Lump;
import ch.retomerz.crime.main.client.map.q3.format.MQ3Map;
import ch.retomerz.crime.main.client.map.q3.format.MQ3MeshVert;
import ch.retomerz.crime.main.client.map.q3.format.MQ3Model;
import ch.retomerz.crime.main.client.map.q3.format.MQ3Node;
import ch.retomerz.crime.main.client.map.q3.format.MQ3Plane;
import ch.retomerz.crime.main.client.map.q3.format.MQ3Texture;
import ch.retomerz.crime.main.client.map.q3.format.MQ3Vertex;
import ch.retomerz.crime.main.client.map.q3.format.MQ3VisData;

import java.util.ArrayList;
import java.util.List;

public class MQ3MapLoader {

    private static final char[] cMagicNumber    = new char[] {'I', 'B', 'S', 'P'};
    private static final int cVersion           = 0x2E;
    private static final int cEntityLump        = 0x00; // Entities : Game-related object descriptions.
    private static final int cTextureLump       = 0x01; // Textures : Surface descriptions.
    private static final int cPlaneLump         = 0x02; // Planes : Planes used by map geometry.
    private static final int cNodeLump          = 0x03; // Nodes : BSP tree nodes.
    private static final int cLeafLump          = 0x04; // Leafs : BSP tree leaves.
    private static final int cLeafFaceLump      = 0x05; // LeafFaces : Lists of face indices, one list per leaf.
    private static final int cLeafBrushLump     = 0x06; // LeafBrushes  Lists of brush indices, one list per leaf.
    private static final int cModelLump         = 0x07; // Models  Descriptions of rigid world geometry in map.
    private static final int cBrushLump         = 0x08; // Brushes  Convex polyhedra used to describe solid space.
    private static final int cBrushSideLump     = 0x09; // Brushsides  Brush surfaces.
    private static final int cVertexLump        = 0x0A; // Vertexes  Vertices used to describe faces.
    private static final int cMeshVertLump      = 0x0B; // MeshVerts  Lists of offsets, one list per mesh.
    private static final int cEffectLump        = 0x0C; // Effects  List of special map effects.
    private static final int cFaceLump          = 0x0D; // Faces  Surface geometry.
    private static final int cLightMapLump      = 0x0E; // LightMaps  Packed lightmap data.
    private static final int cLightVolLump      = 0x0F; // LightVols  Local illumination data.
    private static final int cVisDataLump       = 0x10; // Visdata  Cluster-cluster visibility data.

    public static String readMap(LEDByteAccess _File, MQ3Map _Map) {

        if (!readHeader(_File, _Map)) {
            return "Invalid Q3 map header.";
        }

        readEntity(_File, _Map);
        readTexture(_File, _Map);
        readPlane(_File, _Map);
        readNode(_File, _Map);
        readLeaf(_File, _Map);
        readLeafFace(_File, _Map);
        readLeafBrush(_File, _Map);
        readModel(_File, _Map);
        readBrush(_File, _Map);
        readBrushSide(_File, _Map);
        readVertex(_File, _Map);
        readMeshVert(_File, _Map);
        readEffect(_File, _Map);
        readFace(_File, _Map);
        readLightMap(_File, _Map);
        readLightVol(_File, _Map);
        readVisData(_File, _Map);

        return null;
    }

    private static boolean readHeader(LEDByteAccess _File, MQ3Map _Map) {
        final MQ3Header header = new MQ3Header();
        header.m_MagicNumber = _File.readChar(4);
        header.m_Version = _File.readInt();
        header.m_Lumpes = new MQ3Lump[17];
        for (int i = 0; i < header.m_Lumpes.length; i++) {
            final MQ3Lump lump = new MQ3Lump();
            lump.m_Offset = _File.readInt();
            lump.m_Length = _File.readInt();
            header.m_Lumpes[i] = lump;
        }
        _Map.m_Header = header;

        return isValid(_Map);
    }

    private static void readEntity(LEDByteAccess _File, MQ3Map _Map) {
        final MQ3Entity entity = new MQ3Entity();

        // Set the entity size.
        entity.m_Size = _Map.m_Header.m_Lumpes[cEntityLump].m_Length;

        // Allocate the entity buffer.
        entity.m_Buffer = new char[entity.m_Size];

        // Go to the start of the chunk.
        _File.seek(_Map.m_Header.m_Lumpes[cEntityLump].m_Offset);

        // Read the buffer.
        entity.m_Buffer = _File.readChar(entity.m_Size);

        _Map.m_Entity = entity;
    }

    private static void readTexture(LEDByteAccess _File, MQ3Map _Map) {
        final int count = _Map.m_Header.m_Lumpes[cTextureLump].m_Length / MQ3Texture.SIZE_OF;

        _File.seek(_Map.m_Header.m_Lumpes[cTextureLump].m_Offset);

        MQ3Texture texture;
        for (int i = 0; i < count; ++i) {
            texture = new MQ3Texture();
            texture.m_Name = _File.readChar(64);
            texture.m_Flags = _File.readInt();
            texture.m_Contents = _File.readInt();
            _Map.m_Textures.add(texture);
        }
    }

    private static void readPlane(LEDByteAccess _File, MQ3Map _Map) {
        final int count = _Map.m_Header.m_Lumpes[cPlaneLump].m_Length / MQ3Plane.SIZE_OF;

        _File.seek(_Map.m_Header.m_Lumpes[cPlaneLump].m_Offset);

        MQ3Plane plane;
        for (int i = 0; i < count; ++i) {
            plane = new MQ3Plane();
            plane.m_Normal = _File.readFloat(3);
            plane.m_Distance = _File.readFloat();
            _Map.m_Planes.add(plane);
        }
    }

    private static void readNode(LEDByteAccess _File, MQ3Map _Map) {
        final int count = _Map.m_Header.m_Lumpes[cNodeLump].m_Length / MQ3Node.SIZE_OF;

        _File.seek(_Map.m_Header.m_Lumpes[cNodeLump].m_Offset);

        MQ3Node node;
        for (int i = 0; i < count; ++i) {
            node = new MQ3Node();
            node.m_Plane = _File.readInt();
            node.m_Children = _File.readInt(2);
            node.m_Mins = _File.readInt(3);
            node.m_Maxs = _File.readInt(3);
            _Map.m_Nodes.add(node);
        }
    }

    private static void readLeaf(LEDByteAccess _File, MQ3Map _Map) {
        final int count = _Map.m_Header.m_Lumpes[cLeafLump].m_Length / MQ3Leaf.SIZE_OF;

        _File.seek(_Map.m_Header.m_Lumpes[cLeafLump].m_Offset);

        MQ3Leaf leaf;
        for (int i = 0; i < count; ++i) {
            leaf = new MQ3Leaf();
            leaf.m_Cluster = _File.readInt();
            leaf.m_Area = _File.readInt();
            leaf.m_Mins = _File.readInt(3);
            leaf.m_Maxs = _File.readInt(3);
            leaf.m_LeafFace = _File.readInt();
            leaf.m_NbLeafFaces = _File.readInt();
            leaf.m_LeafBrush = _File.readInt();
            leaf.m_NbLeafBrushes = _File.readInt();
            _Map.m_Leaves.add(leaf);
        }
    }

    private static void readLeafFace(LEDByteAccess _File, MQ3Map _Map) {
        final int count = _Map.m_Header.m_Lumpes[cLeafFaceLump].m_Length / MQ3LeafFace.SIZE_OF;

        _File.seek(_Map.m_Header.m_Lumpes[cLeafFaceLump].m_Offset);

        MQ3LeafFace leafFace;
        for (int i = 0; i < count; ++i) {
            leafFace = new MQ3LeafFace();
            leafFace.m_FaceIndex = _File.readInt();
            _Map.m_LeafFaces.add(leafFace);
        }
    }

    private static void readLeafBrush(LEDByteAccess _File, MQ3Map _Map) {
        final int count = _Map.m_Header.m_Lumpes[cLeafBrushLump].m_Length / MQ3LeafBrush.SIZE_OF;

        _File.seek(_Map.m_Header.m_Lumpes[cLeafBrushLump].m_Offset);

        MQ3LeafBrush leafBrush;
        for (int i = 0; i < count; ++i) {
            leafBrush = new MQ3LeafBrush();
            leafBrush.m_BrushIndex = _File.readInt();
            _Map.m_LeafBrushes.add(leafBrush);
        }
    }

    private static void readModel(LEDByteAccess _File, MQ3Map _Map) {
        final int count = _Map.m_Header.m_Lumpes[cModelLump].m_Length / MQ3Model.SIZE_OF;

        _File.seek(_Map.m_Header.m_Lumpes[cModelLump].m_Offset);

        MQ3Model model;
        for (int i = 0; i < count; ++i) {
            model = new MQ3Model();
            model.m_Mins = _File.readFloat(3);
            model.m_Maxs = _File.readFloat(3);
            model.m_Face = _File.readInt();
            model.m_NbFaces = _File.readInt();
            model.m_Brush = _File.readInt();
            model.m_NBrushes = _File.readInt();
            _Map.m_Models.add(model);
        }
    }

    private static void readBrush(LEDByteAccess _File, MQ3Map _Map) {
        final int count = _Map.m_Header.m_Lumpes[cBrushLump].m_Length / MQ3Brush.SIZE_OF;

        _File.seek(_Map.m_Header.m_Lumpes[cBrushLump].m_Offset);

        MQ3Brush brush;
        for (int i = 0; i < count; ++i) {
            brush = new MQ3Brush();
            brush.m_BrushSide = _File.readInt();
            brush.m_NbBrushSides = _File.readInt();
            brush.m_TextureIndex = _File.readInt();
            _Map.m_Brushes.add(brush);
        }
    }

    private static void readBrushSide(LEDByteAccess _File, MQ3Map _Map) {
        final int count = _Map.m_Header.m_Lumpes[cBrushSideLump].m_Length / MQ3BrushSide.SIZE_OF;

        _File.seek(_Map.m_Header.m_Lumpes[cBrushSideLump].m_Offset);

        MQ3BrushSide brushSide;
        for (int i = 0; i < count; ++i) {
            brushSide = new MQ3BrushSide();
            brushSide.m_PlaneIndex = _File.readInt();
            brushSide.m_TextureIndex = _File.readInt();
            _Map.m_BrushSides.add(brushSide);
        }
    }

    private static void readVertex(LEDByteAccess _File, MQ3Map _Map) {
        final int count = _Map.m_Header.m_Lumpes[cVertexLump].m_Length / MQ3Vertex.SIZE_OF;

        _File.seek(_Map.m_Header.m_Lumpes[cVertexLump].m_Offset);

        MQ3Vertex vertex;
        for (int i = 0; i < count; ++i) {
            vertex = new MQ3Vertex();
            vertex.m_Position = _File.readFloat(3);
            vertex.m_TexCoord = _File.readFloat(2);
            vertex.m_TexCoordLight = _File.readFloat(2);
            vertex.m_Normal = _File.readFloat(3);
            //vertex.m_Color = _File.readCharUnsigned(4);
            vertex.m_Color = _File.readCharUnsigned(4);
          brightnessAdjustVertexTest(vertex.m_Color);
            _Map.m_Vertices.add(vertex);
        }
    }

  /*private static int[] colorToVec(long color) {
    return new int[] {
            (color & 0xFF) / 0xFF),
    (((color & 0xFF00) >> 8) / 0xFF),
            (((color & 0xFF0000) >> 16) / 0xFF),
            1
    };
  }*/

  private static void brightnessAdjustVertexTest(char []color) {
    color[0] = 255;
    color[1] = 255;
    color[2] = 255;
  }

  private static void brightnessAdjustVertexTestX(char []color) {
    int scale = 1;
    int temp;
    int factor = 4;

    color[0] *= factor;
    color[1] *= factor;
    color[2] *= factor;

    if(color[0] > 1 && (temp = 1/color[0]) < scale) { scale = temp; }
    if(color[1] > 1 && (temp = 1/color[1]) < scale) { scale = temp; }
    if(color[2] > 1 && (temp = 1/color[2]) < scale) { scale = temp; }

    color[0] *= scale;
    color[1] *= scale;
    color[2] *= scale;

    //color[0] = 100; // TODO
    //color[1] = 100; // TODO
    //color[2] = 100; // TODO
    color[3] = 1;
  }

  private static int[] brightnessAdjustVertex(int []color, int factor) {
    int scale = 1;
    int temp = 0;

    color[0] *= factor;
    color[1] *= factor;
    color[2] *= factor;

    if(color[0] > 1 && (temp = 1/color[0]) < scale) { scale = temp; }
    if(color[1] > 1 && (temp = 1/color[1]) < scale) { scale = temp; }
    if(color[2] > 1 && (temp = 1/color[2]) < scale) { scale = temp; }

    color[0] *= scale;
    color[1] *= scale;
    color[2] *= scale;

    return color;
  }

    private static void readMeshVert(LEDByteAccess _File, MQ3Map _Map) {
        final int count = _Map.m_Header.m_Lumpes[cMeshVertLump].m_Length / MQ3MeshVert.SIZE_OF;

        _File.seek(_Map.m_Header.m_Lumpes[cMeshVertLump].m_Offset);

        MQ3MeshVert mesh;
        for (int i = 0; i < count; ++i) {
            mesh = new MQ3MeshVert();
            mesh.m_MeshVert = _File.readInt();
            _Map.m_MeshVertices.add(mesh);
        }
    }

    private static void readEffect(LEDByteAccess _File, MQ3Map _Map) {
        final int count = _Map.m_Header.m_Lumpes[cEffectLump].m_Length / MQ3Effect.SIZE_OF;

        _File.seek(_Map.m_Header.m_Lumpes[cEffectLump].m_Offset);

        MQ3Effect effect;
        for (int i = 0; i < count; ++i) {
            effect = new MQ3Effect();
            effect.m_Name = _File.readChar(64);
            effect.m_Brush = _File.readInt();
            effect.m_Unknown = _File.readInt();
            _Map.m_Effects.add(effect);
        }
    }

    private static void readFace(LEDByteAccess _File, MQ3Map _Map) {
        final int count = _Map.m_Header.m_Lumpes[cFaceLump].m_Length / MQ3Face.SIZE_OF;

        _File.seek(_Map.m_Header.m_Lumpes[cFaceLump].m_Offset);

        MQ3Face face;
        for (int i = 0; i < count; ++i) {
            face = new MQ3Face();
            face.m_TextureIndex = _File.readInt();
            face.m_EffectIndex = _File.readInt();
            face.m_Type = _File.readInt();
            face.m_Vertex = _File.readInt();
            face.m_NbVertices = _File.readInt();
            face.m_MeshVertex = _File.readInt();
            face.m_NbMeshVertices = _File.readInt();
            face.m_LightmapIndex = _File.readInt();
            face.m_LightmapCorner = _File.readInt(2);
            face.m_LightmapSize = _File.readInt(2);
            face.m_LightmapOrigin = _File.readFloat(3);
            face.m_LightmapVecs = _File.readFloat2(2, 3);
            face.m_Normal = _File.readFloat(3);
            face.m_PatchSize = _File.readInt(2);
            _Map.m_Faces.add(face);
        }
    }

    private static void readLightMap(LEDByteAccess _File, MQ3Map _Map) {
      int lightmapSize = 128 * 128;
      int count = _Map.m_Header.m_Lumpes[cLightMapLump].m_Length / (lightmapSize*3);

      int gridSize = 2;

      while(gridSize * gridSize < count) {
        gridSize *= 2;
      }

      final int textureSize = gridSize * 128;

      int xOffset = 0;
      int yOffset = 0;

      final List<MQ3LightMapElement> lightmaps = new ArrayList<MQ3LightMapElement>();
      final List<MQ3LightMapElementRect> lightmapRects = new ArrayList<MQ3LightMapElementRect>();
      final int[] rgb = new int[3];

      _File.seek(_Map.m_Header.m_Lumpes[cLightMapLump].m_Offset);

      for(int i = 0; i < count; ++i) {
        final int[] elements = new int[lightmapSize*4];

        for(int j = 0; j < lightmapSize*4; j+=4) {
          rgb[0] = _File.readUByte();
          rgb[1] = _File.readUByte();
          rgb[2] = _File.readUByte();

          brightnessAdjust(rgb, 4);
          //brightnessAdjustTestX(rgb);

          elements[j] = rgb[0];
          elements[j+1] = rgb[1];
          elements[j+2] = rgb[2];
          elements[j+3] = 255;
        }

        lightmaps.add(new MQ3LightMapElement(
                xOffset,
                yOffset,
                128, 128,
                elements
        ));

        lightmapRects.add(new MQ3LightMapElementRect(
                xOffset/(1.0*textureSize),
                yOffset/(1.0*textureSize),
                128.0/textureSize,
                128.0/textureSize
        ));

        xOffset += 128;
        if(xOffset >= textureSize) {
          yOffset += 128;
          xOffset = 0;
        }
      }

      _Map.m_LightMap = new MQ3LightMap(textureSize, lightmaps, lightmapRects);
    }

  static int xxx;
  private static void brightnessAdjustTest(int []color) {
    color[0] = xxx;
    color[1] = xxx;
    color[2] = xxx;
    xxx += 20;
    if (xxx > 255) {
      xxx = 0;
    }
    if (color[0] > 255) {
      color[0] = 255;
    }
    if (color[1] > 255) {
      color[1] = 255;
    }
    if (color[2] > 255) {
      color[2] = 255;
    }
  }

  static void HSVtoRGB( float h, float s, float v, float rgb[] )
  {
    int i;
    float f;
    float p, q, t;

    h *= 5;

    i = floor( h );
    f = h - i;

    p = v * ( 1 - s );
    q = v * ( 1 - s * f );
    t = v * ( 1 - s * ( 1 - f ) );

    switch ( i )
    {
      case 0:
        rgb[0] = v;
        rgb[1] = t;
        rgb[2] = p;
        break;
      case 1:
        rgb[0] = q;
        rgb[1] = v;
        rgb[2] = p;
        break;
      case 2:
        rgb[0] = p;
        rgb[1] = v;
        rgb[2] = t;
        break;
      case 3:
        rgb[0] = p;
        rgb[1] = q;
        rgb[2] = v;
        break;
      case 4:
        rgb[0] = t;
        rgb[1] = p;
        rgb[2] = v;
        break;
      case 5:
        rgb[0] = v;
        rgb[1] = p;
        rgb[2] = q;
        break;
    }
  }

 private static int floor( double x ) {
    return (int)(x + 0x40000000) - 0x40000000;
  }

  private static void brightnessAdjustTestX(int []color) {
    float intensity;
    float out[] = new float[3];

    intensity = 0.33f * color[0] + 0.685f * color[1] + 0.063f * color[2];

    if ( intensity > 255 )
      intensity = 1.0f;
    else
      intensity /= 255.0f;

    HSVtoRGB( intensity, 1.00f, 0.50f, out );
    color[0] = (int)out[0];
    color[1] = (int)out[1];
    color[2] = (int)out[2];
  }

  private static void brightnessAdjust(int []color, int factor) {
    double scale = 1;
    double temp = 0;

    color[0] *= factor;
    color[1] *= factor;
    color[2] *= factor;

    if(color[0] > 255 && (temp = 255.0/color[0]) < scale) { scale = temp; }
    if(color[1] > 255 && (temp = 255.0/color[1]) < scale) { scale = temp; }
    if(color[2] > 255 && (temp = 255.0/color[2]) < scale) { scale = temp; }

    color[0] *= scale;
    color[1] *= scale;
    color[2] *= scale;
  }

  /*
        var Q3LightMap = function (buffer) {
            var gamma = 2.7 / 255;
            this.data = new Int32Array(49152);
            buffer.getBlock(this.data, 0, this.data.length);
            this.populateTexture = function (target) {
                var temp;
                var targetData = target.getData();
                for (var i = 0; i < 16384; i++) {
                    var r = gamma * (0xFF & this.data[i * 3]);
                    var g = gamma * (0xFF & this.data[i * 3 + 1]);
                    var b = gamma * (0xFF & this.data[i * 3 + 2]);
                    var scale = 1.0;
                    if (r > 1.0 && (temp = (1.0 / r)) < scale) {
                        scale = temp;
                    }
                    if (g > 1.0 && (temp = (1.0 / g)) < scale) {
                        scale = temp;
                    }
                    if (b > 1.0 && (temp = (1.0 / b)) < scale) {
                        scale = temp;
                    }
                    scale *= 255.0;
                    targetData[i] = (r * scale & 0xFF) << 16 | (g * scale & 0xFF) << 8 | (b * scale & 0xFF);
                }
                return target;
            }
        };
   */
    /*private static void _readLightMap(LEDByteAccess _File, MQ3Map _Map) {
        final int count = _Map.m_Header.m_Lumpes[cLightMapLump].m_Length / MQ3LightMap.SIZE_OF;

        _File.seek(_Map.m_Header.m_Lumpes[cLightMapLump].m_Offset);

        MQ3LightMap lightMap;
        for (int i = 0; i < count; ++i) {
            lightMap = new MQ3LightMap();
            lightMap.m_MapData = _File.readChar3Unsigned(128, 128, 3);
            _Map.m_LightMaps.add(lightMap);
        }
    }*/

    private static void readLightVol(LEDByteAccess _File, MQ3Map _Map) {
        final int count = _Map.m_Header.m_Lumpes[cLightVolLump].m_Length / MQ3LightVol.SIZE_OF;

        _File.seek(_Map.m_Header.m_Lumpes[cLightVolLump].m_Offset);

        MQ3LightVol lightVol;
        for (int i = 0; i < count; ++i) {
            lightVol = new MQ3LightVol();
            lightVol.m_Ambient = _File.readCharUnsigned(3);
            lightVol.m_Directional = _File.readCharUnsigned(3);
            lightVol.m_Dir = _File.readCharUnsigned(2);
            _Map.m_LightVols.add(lightVol);
        }
    }

    private static void readVisData(LEDByteAccess _File, MQ3Map _Map) {
        _File.seek(_Map.m_Header.m_Lumpes[cVisDataLump].m_Offset);

        final MQ3VisData visData = new MQ3VisData();
        visData.m_NbClusters = _File.readInt();
        visData.m_BytesPerCluster = _File.readInt();

        final int bufferSize = visData.m_NbClusters * visData.m_BytesPerCluster;
        visData.m_Buffer = _File.readCharUnsigned(bufferSize);

        _Map.m_VisData = visData;
    }

    private static boolean isValid(MQ3Map _Map) {
        // Check if the header is equal to ID Software Magic Number.
        if (!strncmp(_Map.m_Header.m_MagicNumber, cMagicNumber, 4)) {
            return false;
        }

        // Check if the version number is equal to the Q3 map.
        if (_Map.m_Header.m_Version != cVersion) {
            return false;
        }

        return true;
    }

    private static boolean strncmp(char[] _CharsA, char[] _CharsB, int _Count) {
        for (int i = 0; i < _Count; i++) {
            if (_CharsA[i] != _CharsB[i]) {
                return false;
            }
        }
        return true;
    }

}