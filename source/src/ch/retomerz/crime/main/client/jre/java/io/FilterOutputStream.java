package ch.retomerz.crime.main.client.jre.java.io;

public class FilterOutputStream extends OutputStream {
    protected OutputStream out;

    public FilterOutputStream(OutputStream out) {
        this.out = out;
    }

    @Override
    public void write(int b) {
        out.write(b);
    }

    @Override
    public void write(byte b[]) {
        write(b, 0, b.length);
    }

    @Override
    public void write(byte b[], int off, int len) {
        if ((off | len | (b.length - (len + off)) | (off + len)) < 0)
            throw new IndexOutOfBoundsException();

        for (int i = 0 ; i < len ; i++) {
            write(b[off + i]);
        }
    }

    @Override
    public void flush() {
        out.flush();
    }

    @Override
    public void close() {
        flush();
        out.close();
    }

}