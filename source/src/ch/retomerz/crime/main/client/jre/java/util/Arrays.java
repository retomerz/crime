/**
 * Created by merz on 27.04.14.
 */
package ch.retomerz.crime.main.client.jre.java.util;

public class Arrays {

    public static byte[] copyOf(byte[] original, int newLength) {
        byte[] copy = new byte[newLength];
        System.arraycopy(original, 0, copy, 0,
                Math.min(original.length, newLength));
        return copy;
    }

}