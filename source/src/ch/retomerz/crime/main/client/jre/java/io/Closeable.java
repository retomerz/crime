package ch.retomerz.crime.main.client.jre.java.io;

public interface Closeable {

    void close();

}