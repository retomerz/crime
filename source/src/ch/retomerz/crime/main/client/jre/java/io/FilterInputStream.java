package ch.retomerz.crime.main.client.jre.java.io;

public class FilterInputStream extends InputStream {
    /**
     * The input stream to be filtered.
     */
    protected volatile InputStream in;

    protected FilterInputStream(InputStream in) {
        this.in = in;
    }

    @Override
    public int read() {
        return in.read();
    }

    @Override
    public int read(byte b[]) {
        return read(b, 0, b.length);
    }

    @Override
    public int read(byte b[], int off, int len) {
        return in.read(b, off, len);
    }

    @Override
    public long skip(long n) {
        return in.skip(n);
    }

    @Override
    public int available() {
        return in.available();
    }

    @Override
    public void close() {
        in.close();
    }

    @Override
    public void mark(int readlimit) {
        in.mark(readlimit);
    }

    @Override
    public void reset() {
        in.reset();
    }

    @Override
    public boolean markSupported() {
        return in.markSupported();
    }

}