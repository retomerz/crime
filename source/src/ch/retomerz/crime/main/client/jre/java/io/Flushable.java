package ch.retomerz.crime.main.client.jre.java.io;

public interface Flushable {

    void flush();

}