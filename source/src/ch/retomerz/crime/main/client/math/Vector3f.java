/**
 * Created by merz on 18.05.14.
 */
package ch.retomerz.crime.main.client.math;

public final class Vector3f {

  private Vector3f() {
  }

  public static void zero(final float[] a) {
    a[0] = 0;
    a[1] = 0;
    a[2] = 0;
  }

  public static void set(final float[] a, final float[] b) {
    b[0] = a[0];
    b[1] = a[1];
    b[2] = a[2];
    //return b
  }

  public static void scale(final float[] a, final float factor) {
    a[0] *= factor;
    a[1] *= factor;
    a[2] *= factor;
  }

  public static void scale(final float[] a, final float factor, final float[] b) {
    b[0] = a[0] * factor;
    b[1] = a[1] * factor;
    b[2] = a[2] * factor;
  }

  public static void cross(final float[] a, final float[] b, final float[] c) {
    final float d = a[0],
            e = a[1],
            aa = a[2],
            g = b[0],
            f = b[1],
            bb = b[2];
    c[0] = e * bb - aa * f;
    c[1] = aa * g - d * bb;
    c[2] = d * f - e * g;
    //return c
  }

  public static void add(final float[] a, final float[] b) {
    a[0] += b[0];
    a[1] += b[1];
    a[2] += b[2];
  }

  public static void add(final float[] a, final float[] b, final float[] c) {
    c[0] = a[0] + b[0];
    c[1] = a[1] + b[1];
    c[2] = a[2] + b[2];
    //return c
  }

  public static void subtract(final float[] a, final float[] b, final float[] c) {
    c[0] = a[0] - b[0];
    c[1] = a[1] - b[1];
    c[2] = a[2] - b[2];
  }

  public static void normalize(final float[] a) {
    final float
            c = a[0],
            d = a[1],
            e = a[2];

    float g = (float)Math.sqrt(c * c + d * d + e * e);
    if (g>0) {
      if (1 == g) {
        a[0] = c;
        a[1] = d;
        a[2] = e;
      } else {
        g = 1 / g;
        a[0] = c * g;
        a[1] = d * g;
        a[2] = e * g;
      }
    } else {
      a[0] = 0;
      a[1] = 0;
      a[2] = 0;
    }
  }

  public static void normalize(final float[] a, final float[] b) {
    final float
            c = a[0],
            d = a[1],
            e = a[2];
    float g = (float)Math.sqrt(c * c + d * d + e * e);
    if (g>0) {
      if (1 == g) {
        b[0] = c;
        b[1] = d;
        b[2] = e;
      } else {
        g = 1 / g;
        b[0] = c * g;
        b[1] = d * g;
        b[2] = e * g;
      }
    } else {
      b[0] = 0;
      b[1] = 0;
      b[2] = 0;
    }
  }

  public static float dot(float[] a, float[] b) {
    return a[0] * b[0] + a[1] * b[1] + a[2] * b[2];
  }

  public static float length(float[] a) {
    final float b = a[0],
            c = a[1],
            aa = a[2];
    return (float)Math.sqrt(b * b + c * c + aa * aa);
  }

}