/**
 * Created by merz on 14.05.14.
 */
package ch.retomerz.crime.main.client.math;

public final class Matrix4f {

  public final float[] a = new float[16];

  public Matrix4f() {
  }

  public void identity() {
    a[0] = 1;
    a[1] = 0;
    a[2] = 0;
    a[3] = 0;
    a[4] = 0;
    a[5] = 1;
    a[6] = 0;
    a[7] = 0;
    a[8] = 0;
    a[9] = 0;
    a[10] = 1;
    a[11] = 0;
    a[12] = 0;
    a[13] = 0;
    a[14] = 0;
    a[15] = 1;
  }

  public void rotateX(final float angle) {
    final float
            d = (float)Math.sin(angle),
            b = (float)Math.cos(angle),
            e = a[4],
            g = a[5],
            f = a[6],
            h = a[7],
            i = a[8],
            j = a[9],
            k = a[10],
            l = a[11];

    a[4] = e * b + i * d;
    a[5] = g * b + j * d;
    a[6] = f * b + k * d;
    a[7] = h * b + l * d;
    a[8] = e * -d + i * b;
    a[9] = g * -d + j * b;
    a[10] = f * -d + k * b;
    a[11] = h * -d + l * b;
  }

  public void rotateZ(final float angle) {
    final float
            d = (float)Math.sin(angle),
            b = (float)Math.cos(angle),
            e = a[0],
            g = a[1],
            f = a[2],
            h = a[3],
            i = a[4],
            j = a[5],
            k = a[6],
            l = a[7];

    a[0] = e * b + i * d;
    a[1] = g * b + j * d;
    a[2] = f * b + k * d;
    a[3] = h * b + l * d;
    a[4] = e * -d + i * b;
    a[5] = g * -d + j * b;
    a[6] = f * -d + k * b;
    a[7] = h * -d + l * b;
  }

  public void translate(final float x, final float y, final float z) {
    a[12] = a[0] * x + a[4] * y + a[8] * z + a[12];
    a[13] = a[1] * x + a[5] * y + a[9] * z + a[13];
    a[14] = a[2] * x + a[6] * y + a[10] * z + a[14];
    a[15] = a[3] * x + a[7] * y + a[11] * z + a[15];
  }

  public void perspective(float fov, float ratio, float nearDistance, float farDistance) {
    fov = (float)(nearDistance * Math.tan(fov * Math.PI / 360));
    ratio *= fov;
    frustum(-ratio, ratio, -fov, fov, nearDistance, farDistance /* ,this */);
  }

  public void frustum(float left, float right, float bottom, float top, float nearDistance, float farDistance) {
    final float h = right - left,
            i = top - bottom,
            j = farDistance - nearDistance;
    a[0] = 2 * nearDistance / h;
    a[1] = 0;
    a[2] = 0;
    a[3] = 0;
    a[4] = 0;
    a[5] = 2 * nearDistance / i;
    a[6] = 0;
    a[7] = 0;
    a[8] = (right + left) / h;
    a[9] = (top + bottom) / i;
    a[10] = -(farDistance + nearDistance) / j;
    a[11] = -1;
    a[12] = 0;
    a[13] = 0;
    a[14] = -(2 * farDistance * nearDistance) / j;
    a[15] = 0;
  }

  public void inverse() {
    final float
            c = a[0],
            d = a[1],
            e = a[2],
            g = a[3],
            f = a[4],
            h = a[5],
            i = a[6],
            j = a[7],
            k = a[8],
            l = a[9],
            n = a[10],
            o = a[11],
            m = a[12],
            p = a[13],
            r = a[14],
            s = a[15],
            A = c * h - d * f,
            B = c * i - e * f,
            t = c * j - g * f,
            u = d * i - e * h,
            v = d * j - g * h,
            w = e * j - g * i,
            x = k * p - l * m,
            y = k * r - n * m,
            z = k * s - o * m,
            C = l * r - n * p,
            D = l * s - o * p,
            E = n * s - o * r;
    float q = A * E - B * D + t * C + u * z - v * y + w * x;

    if (q > 0) {
      q = 1 / q;
      a[0] = (h * E - i * D + j * C) * q;
      a[1] = (-d * E + e * D - g * C) * q;
      a[2] = (p * w - r * v + s * u) * q;
      a[3] = (-l * w + n * v - o * u) * q;
      a[4] = (-f * E + i * z - j * y) * q;
      a[5] = (c * E - e * z + g * y) * q;
      a[6] = (-m * w + r * t - s * B) * q;
      a[7] = (k * w - n * t + o * B) * q;
      a[8] = (f * D - h * z + j * x) * q;
      a[9] = (-c * D + d * z - g * x) * q;
      a[10] = (m * v - p * t + s * A) * q;
      a[11] = (-k * v + l * t - o * A) * q;
      a[12] = (-f * C + h * y - i * x) * q;
      a[13] = (c * C - d * y + e * x) * q;
      a[14] = (-m * u + p * B - r * A) * q;
      a[15] = (k * u - l * B + n * A) * q;
    }
  }

  public static void multiply3f(final Matrix4f a, final float[] b) {
    final float
            d = b[0],
            e = b[1],
            bb = b[2];
    b[0] = a.a[0] * d + a.a[4] * e + a.a[8] * bb + a.a[12];
    b[1] = a.a[1] * d + a.a[5] * e + a.a[9] * bb + a.a[13];
    b[2] = a.a[2] * d + a.a[6] * e + a.a[10] * bb + a.a[14];
    //return b
  }

}