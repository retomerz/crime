/**
 * Created by merz on 18.05.14.
 */
package ch.retomerz.crime.main.client.input;

import com.google.gwt.dom.client.CanvasElement;
import com.google.gwt.dom.client.NativeEvent;

public final class BrowserMouse {

  public BrowserMouse() {
  }

  private native boolean isCursorCatchedJSNI () /*-{
      if(!navigator.pointer) {
          navigator.pointer = navigator.webkitPointer || navigator.mozPointer;
      }
      if(navigator.pointer) {
          if(typeof(navigator.pointer.isLocked) === "boolean") {
              // Chrome initially launched with this interface
              return navigator.pointer.isLocked;
          } else if(typeof(navigator.pointer.isLocked) === "function") {
              // Some older builds might provide isLocked as a function
              return navigator.pointer.isLocked();
          } else if(typeof(navigator.pointer.islocked) === "function") {
              // For compatibility with early Firefox build
              return navigator.pointer.islocked();
          }
      }
      return false;
  }-*/;

  private native void setCursorCatchedJSNI(CanvasElement element) /*-{
      // Navigator pointer is not the right interface according to spec.
      // Here for backwards compatibility only
      if(!navigator.pointer) {
          navigator.pointer = navigator.webkitPointer || navigator.mozPointer;
      }
      // element.requestPointerLock
      if(!element.requestPointerLock) {
          element.requestPointerLock = (function() {
              return  element.webkitRequestPointerLock ||
                  element.mozRequestPointerLock    ||
                  function(){
                      if(navigator.pointer) {
                          navigator.pointer.lock(element);
                      }
                  };
          })();
      }
      element.requestPointerLock();
  }-*/;

  private native void exitCursorCatchedJSNI () /*-{
      if(!$doc.exitPointerLock) {
          $doc.exitPointerLock = (function() {
              return  $doc.webkitExitPointerLock ||
                  $doc.mozExitPointerLock ||
                  function(){
                      if(navigator.pointer) {
                          var elem = this;
                          navigator.pointer.unlock();
                      }
                  };
          })();
      }
  }-*/;

  public native float getMovementXJSNI(NativeEvent event) /*-{
      return event.movementX || event.webkitMovementX || 0;
  }-*/;

  public native float getMovementYJSNI(NativeEvent event) /*-{
      return event.movementY || event.webkitMovementY || 0;
  }-*/;

  public void setCursorCatched(CanvasElement element, boolean catched) {
    if (catched)
      setCursorCatchedJSNI(element);
    else
      exitCursorCatchedJSNI();
  }

  public boolean isCursorCatched () {
    return isCursorCatchedJSNI();
  }

}