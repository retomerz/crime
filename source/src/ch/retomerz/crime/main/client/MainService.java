package ch.retomerz.crime.main.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("MainService")
public interface MainService extends RemoteService {

    String getResourceUrl(String _Name);

    byte[] getByteData(String path);

    String getBSP(String _Name);

    public static class App {
        private static MainServiceAsync ourInstance = GWT.create(MainService.class);
        public static synchronized MainServiceAsync getInstance() {
            return ourInstance;
        }
    }
}
