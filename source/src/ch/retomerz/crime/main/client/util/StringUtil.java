/**
 * Created by merz on 21.04.14.
 */
package ch.retomerz.crime.main.client.util;

import java.io.UnsupportedEncodingException;

public final class StringUtil {

    private StringUtil() {
    }

    public static String fromUS_ASCII(byte [] data) {
        try {
            return new String(data, "US-ASCII");
        } catch (UnsupportedEncodingException e) {
            throw new Error(e);
        }
    }

}