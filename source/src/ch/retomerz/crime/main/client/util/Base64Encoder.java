/**
 * Created by merz on 03.05.14.
 */
package ch.retomerz.crime.main.client.util;

import ch.retomerz.crime.main.client.jre.java.util.Arrays;

import java.io.UnsupportedEncodingException;

public final class Base64Encoder {

    private Base64Encoder() {
    }

    static final char[] MAP = {
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
            'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
            'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '-', '_'
    };

    public static byte[] encode(byte[] src) {
        final byte[] dst = new byte[4 * ((src.length + 2) / 3)];
        final int ret = encode0(src, 0, src.length, dst);
        if (ret != dst.length)
            return Arrays.copyOf(dst, ret);
        return dst;
    }

    public static String encodeToString(byte[] src) {
        try {
            return new String(encode(src), "US-ASCII");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    private static int encode0(final byte[] src, final int off, final int end, final byte[] dst) {
        final char[] base64 = MAP;
        int sp = off;
        final int slen = (end - off) / 3 * 3;
        final int sl = off + slen;
        int dp = 0;
        while (sp < sl) {
            int sl0 = Math.min(sp + slen, sl);
            for (int sp0 = sp, dp0 = dp ; sp0 < sl0; ) {
                int bits = (src[sp0++] & 0xff) << 16 |
                        (src[sp0++] & 0xff) <<  8 |
                        (src[sp0++] & 0xff);
                dst[dp0++] = (byte)base64[(bits >>> 18) & 0x3f];
                dst[dp0++] = (byte)base64[(bits >>> 12) & 0x3f];
                dst[dp0++] = (byte)base64[(bits >>> 6)  & 0x3f];
                dst[dp0++] = (byte)base64[bits & 0x3f];
            }
            dp += ((sl0 - sp) / 3 * 4);
            sp = sl0;
        }
        if (sp < end) {               // 1 or 2 leftover bytes
            int b0 = src[sp++] & 0xff;
            dst[dp++] = (byte)base64[b0 >> 2];
            if (sp == end) {
                dst[dp++] = (byte)base64[(b0 << 4) & 0x3f];
                dst[dp++] = '=';
                dst[dp++] = '=';
            } else {
                int b1 = src[sp] & 0xff;
                dst[dp++] = (byte)base64[(b0 << 4) & 0x3f | (b1 >> 4)];
                dst[dp++] = (byte)base64[(b1 << 2) & 0x3f];
                dst[dp++] = '=';
            }
        }
        return dp;
    }

}