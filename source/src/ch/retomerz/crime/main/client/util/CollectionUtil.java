/**
 * Created by merz on 23.04.14.
 */
package ch.retomerz.crime.main.client.util;

import java.util.Collection;

public final class CollectionUtil {

    private CollectionUtil() {
    }

    public static int[] integerToArray(Collection<Integer> _Collection) {
        int[] ret = new int[_Collection.size()];
        int i = 0;
        for (Integer value : _Collection) {
            ret[i] = value;
            i++;
        }
        return ret;
    }

    public static float[] floatToArray(Collection<Float> _Collection) {
        float[] ret = new float[_Collection.size()];
        int i = 0;
        for (Float value : _Collection) {
            ret[i] = value;
            i++;
        }
        return ret;
    }

}