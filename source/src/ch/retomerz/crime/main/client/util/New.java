/**
 * Created by merz on 23.04.14.
 */
package ch.retomerz.crime.main.client.util;

import java.util.ArrayList;

public final class New {

    private static final int LIST_CAPACITY = 2;

    private New() {
    }

    public static <T> ArrayList<T> arrayList() {
        return new ArrayList<T>(LIST_CAPACITY);
    }

}