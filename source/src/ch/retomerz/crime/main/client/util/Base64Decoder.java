/**
 * Created by merz on 03.05.14.
 */
package ch.retomerz.crime.main.client.util;

import ch.retomerz.crime.main.client.jre.java.util.Arrays;

import java.io.UnsupportedEncodingException;

public final class Base64Decoder {

    private Base64Decoder() {
    }

    private static final int[] MAP = new int[256];

    static {
        for (int i = 0; i < MAP.length; i++)
            MAP[i] = -1;
        for (int i = 0; i < Base64Encoder.MAP.length; i++)
            MAP[Base64Encoder.MAP[i]] = i;
        MAP['='] = -2;
    }

    public static byte[] decode(byte[] src) {
        byte[] dst = new byte[outLength(src, 0, src.length)];
        int ret = decode0(src, 0, src.length, dst);
        if (ret != dst.length) {
            dst = Arrays.copyOf(dst, ret);
        }
        return dst;
    }

    public static byte[] decode(String src) {
        try {
            return decode(src.getBytes("US-ASCII"));
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    private static int outLength(final byte[] src, final int sp, final int sl) {
        int paddings = 0;
        final int len = sl - sp;
        if (len == 0)
            return 0;
        if (len < 2)
            throw new IllegalArgumentException("Input byte[] should at least have 2 bytes for base64 bytes");
        if (src[sl - 1] == '=') {
            paddings++;
            if (src[sl - 2] == '=')
                paddings++;
        }
        if (paddings == 0 && (len & 0x3) !=  0)
            paddings = 4 - (len & 0x3);
        return 3 * ((len + 3) / 4) - paddings;
    }

    private static int decode0(final byte[] src, int sp, final int sl, final byte[] dst) {
        final int[] base64 = MAP;
        int dp = 0;
        int bits = 0;
        int shiftto = 18;       // pos of first byte of 4-byte atom
        boolean padding = false;
        while (sp < sl) {
            int b = src[sp++] & 0xff;
            if ((b = base64[b]) < 0) {
                if (b == -2) {     // padding byte
                    padding = true;
                    break;
                }
                throw new IllegalArgumentException("Illegal base64 character " + Integer.toString(src[sp - 1], 16));
            }
            bits |= (b << shiftto);
            shiftto -= 6;
            if (shiftto < 0) {
                dst[dp++] = (byte)(bits >> 16);
                dst[dp++] = (byte)(bits >>  8);
                dst[dp++] = (byte)(bits);
                shiftto = 18;
                bits = 0;
            }
        }
        // reach end of byte arry or hit padding '=' characters.
        // if '=' presents, they must be the last one or two.
        if (shiftto == 6) {           // xx==
            if (padding && (sp + 1 != sl || src[sp] != '='))
                throw new IllegalArgumentException("Input byte array has wrong 4-byte ending unit");
            dst[dp++] = (byte)(bits >> 16);
        } else if (shiftto == 0) {    // xxx=
            if (padding && sp != sl)
                throw new IllegalArgumentException("Input byte array has wrong 4-byte ending unit");
            dst[dp++] = (byte)(bits >> 16);
            dst[dp++] = (byte)(bits >>  8);
        } else if (padding || shiftto != 18) {
            throw new IllegalArgumentException("last unit does not have enough bytes");
        }
        return dp;
    }

}