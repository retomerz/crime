/**
 * Created by merz on 21.05.14.
 */
package ch.retomerz.crime.main.client.util;

import com.google.gwt.core.client.JavaScriptObject;
import com.googlecode.gwtgl.array.Uint8Array;

public class JSArrayUtil extends JavaScriptObject {

  protected JSArrayUtil() {
  }

  public static Uint8Array createArray(int[] _Data) {
    final StringBuilder s = new StringBuilder("var a = new Uint8Array([");
    for (int i = 0; i < _Data.length; i++) {
      if (i>0) {
        s.append(",");
      }
      s.append(_Data[i]);
    }
    s.append("]);");
    Uint8Array ret = createArrayImpl(s.toString());
    return ret;
  }

  private static native Uint8Array createArrayImpl(String s) /*-{
    eval(s);
    return a;
  }-*/;

}